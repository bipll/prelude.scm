Pattern-matching is easily implemented in R5RS-Scheme with the aid of syntax definitions. This text is about pattern-matching introduced in prelude.scm.

## I. Patterns
Patterns are syntactic structures. They are often composed of sub-patterns and thus form pattern trees, which is common for all Scheme structures. Pattern trees are not identical to syntactic trees, namely, escape pattern `(>- <expr>)` is a single atom pattern, i.e. a leaf node. Patterns can be refutable or irrefutable. When a pattern is irrefutable it matches any given expression; refutable pattern can mismatch an expression. What happens then, depends on a pattern-matching form used.

Objects are matched against patterns. When an object matches the pattern, it means that object's type matches pattern shape and all the subobjects match the subpatterns, if any.

The following types of patterns are recognized in prelude.scm's pattern-matching syntaxes.

### 1. Wildcard
A wildcard pattern is a literal identifier `_`. It is irrefutable, i.e. it always matches any expression; it is not a variable, so you cannot use it in further expressions. You can have as many wildcard atoms in your pattern tree as you like; they all would match totally unrelated (sub...)expressions.

When the whole pattern in a pattern-matching syntax is a wildcard, it is not even checked, whether expression can be evaluated at this point; this optimization can skip errors, undefined identifiers, etc.
```scm

#### Example:
(<-! _ x)                           ===> #t
(define l '(1 2 3))
(<-equal? (_ _ _) l)                ===> #t
```
### 2. Empty list
Empty list pattern is a pair of parentheses: `()`. It matches empty list and nothing else.

#### Example:
```scm
(<-equal? () (list))                ===> #t
(<-equal? () 42)                    ===> #f
```
### 3. Literals
Literal patterns are expressions that "evaluate to themselves". In R5RS those are `#f`, `#t`, numbers, characters and strings. Literal patterns are highly refutable; they only match expressions that are (equal?) to them.

#### Example:
```scm
(<-! "hello" 'hello)                                    ===> #f
(<-! "hello" (string-append "hell" "o"))                ===> #t
```
Note that quoted expressions are not literal patterns. For example, if you write 'a in a pattern, this will internally expand to (quote a) at syntactic level and thus turn into two-element list pattern consisting of two pattern variables, quote and a.
```scm
(<-let (('a '(1 2))) (show a quote))                    ===> "21"
```
### 4. Variables
Pattern variables are identifiers. Their exact meaning varies between different pattern-matching syntaxes. They are irrefutable, except for when used in `(<-equal?)`. When they are irrefutable, the value of a variable is set to the corresponding (sub...)expression in case of successful match. Whether the variable should have already been `(define)`d in the current environment, again, depends on the form being invoked.

#### Example:
```scm
(<-let ((a 42)) a)                ===> 42
;(println a)                      ===> error: a not defined
(define a #f)
(<-equal? a #f)                   ===> #t
(<-! a 42)                        ===> #t
a                                 ===> 42
(<-equal? a #f)                   ===> #f
```
### 5. Pairs
Pair patterns are either syntactic lists:
```scm
(<pattern>...)
```
or syntactic improper lists:
```scm
(<pattern1> <pattern2>... . <pattern>)
```
List patterns match lists of the same length, where every subpattern matches corresponding list element.

Improper list patterns match lists or improper lists of (improper) length n or more, where n is amount of subpatterns to the left of the dot, provided `<pattern1>`, `<pattern2>`... match corresponding elements of the (improper) list and `<pattern>` matches *n*th tail.

#### Example:
```scm
(<-if-let (((a b c) '(1 2 3))) (show a b c) 'no-match)                    ===> "123"
(<-if-let (((a b . c) '(1 2 3))) (show a b c) 'no-match)                  ===> "12(3)"
(<-if-let (((a b c) '(1 2))) (show a b c) 'no-match)                      ===> no-match
(<-if-let (((a b . c) '(1 2))) (show a b c) 'no-match)                    ===> "12()"
(<-if-let (((a b c) '(1 2 3 4))) (show a b c) 'no-match)                  ===> no-match
(<-if-let (((a b . c) '(1 2 3 4))) (show a b c) 'no-match)                ===> "12(3 4)"
(<-if-let (((a b c) 42)) (show a b c) 'no-match)                          ===> no-match
(<-if-let (((a b . c) 42)) (show a b c) 'no-match)                        ===> no-match
```
### 6. Synonyms
auxiliary syntax: `(<pattern1> @ <pattern2>)`

auxiliary syntax: `(<pattern1> @ <pattern_2> ... <pattern_n>)`

auxiliary syntax: `(<pattern1> ... <pattern_k> @ <pattern_k+1>)`

auxiliary syntax: `(<pattern1> ... <pattern_k> @ <pattern_k+1> ... <pattern_n>)`

A synonym pattern is composed from two subpatterns via infix literal @. An object matches synonym pattern iff it matches both its constituting parts. As a shorthand, there can be more than two subpatterns with @ inside the parentheses; these cases are interpreted as follows:
```scm
(p @ p1 p2 p3...)                   ---> (p @ (p1 p2 p3 ...))
(p1 ... p_n-1 p_n @ p)              ---> (p1 ... p_n-1 . (p_n @ p))
(p1 ... p_k-1 p_k @ p_k+1 p_k+2...) ---> (p1 ... p_k-1 . (p_k @ p_k+1 p_k+2...))
```
In other words, `@` inside a list pattern always denotes that the closest pattern to the left is matched against the whole list tail; and if a single pattern lies to the right of `@`, it is still the whole list tail, otherwise, when more than one pattern is found to the right of `@` before closing parenthesis, those patterns form list pattern and should be matched elementwise.

Note that the syntax of `@` differs from that in Haskell. In Haskell, left operand of syntactic infix `@` is always a variable; in prelude.scm, this can be an arbitrary pattern.

#### Example:
```scm
(<-let (((a b c @ d e f) '(1 2 3 4 5))) (show a b c d e f))    ===> "12(3 4 5)345"
(<-let (((a b c @ d) '(1 2 3 4))) (show a b c d))              ===> "12(3 4)(3 4)"
(<-let (((v @ #(x y z)) '#(0 3.141593 42))) (show v " has coordinates {" x ", " y ", " z "}."))
                                                               ===> "#(0 3.141593 42) has coordinates {0, 3.141593, 42}."
(<-let ((((head . tail) @ (car cadr caddr cadddr . cddddr)) '(0 1 2 3 4 5 6)))
   (show head ":" tail " in R5RS is decomposed to (" car " " cadr " " caddr " " cadddr " . " cddddr ")."))
                                                               ===> "0:(1 2 3 4 5 6) in R5RS is decomposed to (0 1 2 3 . (4 5 6))."
```
Assume we have a few variables defined:
```scm
(define a #f) (define b #f) (define c #f) (define d #f)
```
This is how they all can be set to the same value in a single statement:
```scm
(<-! (a @ (b @ (c @ d))) 42)                     ===> #t
(list a b c d)                                   ===> (42 42 42 42)
```
(You can, of course, set them all to different values:
```scm
(<-! (a b c d) '(1 2 3 4))                       ===> #t
(list a b c d)                                   ===> (1 2 3 4)
```
This how you can check whether they all are `(equal?)` to the same value, like you would do with numbers and `(=)`:
```scm
(<-equal? (a @ (b @ (c @ d))) 42)                ===> #f
```
### 7. Vectors

auxiliary syntax: `#(<pattern>...)`

auxiliary syntax: `#(<pattern1>... : <pattern>)`

auxiliary syntax: `#(<pattern1>... : <pattern> : <pattern2>...)`

Vector patterns are composite patterns that are syntactic vectors with one notable addition: the :-syntax. The most general form of a vector pattern is:
```scm
#(<pattern_l1>... : <pattern_m> : <pattern_r1>...)
```
For this shape of vector pattern, with _k_ subpatterns to the left of the left colon, and _m_ subpatterns to the right of the right colon, matching rules are as follows:

a) it matches a vector of `(vector-length)` of at least *k*+*m*;
b) `<pattern_l1>`, ..., `<pattern_lk>` match the 0th, ..., (*k*-1)th elements of the vector respectively (the `init`);
c) `<pattern_r1>`, ..., `<pattern_rm>` match the (*n*-*m*)th, ..., (*n*-1)th elements of the vector respectively (the `tail`), where *n* is the (vector-length) of the vector;
d) `<pattern_m>` matches the (subvector) of the vector from indices *k* to (*n*-*m*-1), inclusively (the `middle`).

There can be zero patterns to the left of the left colon or/and to the right of the right colon; then `init` or `tail`, respectively, is assumed to be empty. The pattern
```scm
#(: tail :)
```
is analogous to
```scm
tail
```
but bears additional information that tail should be a vector (i.e. `(tail ? (vector? tail))` in terms of guards). If only one colon is given it is assumed to be the left colon, and thus there should be exactly one subpattern to the right of it, the tail of the vector (we cannot call it `middle` since there is nothing past its right end).

If there is no colon, then vector pattern matches vectors with exactly the same amount of elements as there are subpatterns, provided all subpatterns match corresponding vector elements.

#### Example:
```scm
(<-let ((#(a b c) '#(1 2 3))) (show a b c))                            ===> "123"
(<-let ((#(a b : c) '#(1 2 3))) (show a b c))                          ===> "12#(3)"
(<-let ((#(a b : c) '#(1 2))) (show a b c))                            ===> "12#()"
(<-let ((#(a b : c) '#(1 2 3 4))) (show a b c))                        ===> "12#(3 4)"
(<-let ((#(a b : c : d) '#(1 2 3))) (show a b c d))                    ===> "12#()3"
(<-let ((#(a b : c : d) '#(1 2 3 4))) (show a b c d))                  ===> "12#(3)4"
(<-let ((#(a b : c : d) '#(1 2 3 4 5))) (show a b c d))                ===> "12#(3 4)5"
(<-let ((#(: c : d) '#(1 2 3 4 5))) (show c d))                        ===> "#(1 2 3 4)5"
(<-let ((#(: c : d) '#(1))) (show c d))                                ===> "#()1"
(<-let ((#(: c :) '#(1 2 3))) (show c))                                ===> "#(1 2 3)"
(<-let ((#(: c :) '#())) (show c))                                     ===> "#()"

(<-if-let ((#(1 : b) '#(1 2 3))) (show b) 'no-match)                   ===> "#(2 3)"
(<-if-let ((#(1 : b) '#(2 3 4))) (show b) 'no-match)                   ===> no-match
(<-if-let ((#(1 : b) 42)) (show b) 'no-match)                          ===> no-match
```
The functions `(vector-car)` and `(vector-cdr)`, vector analogs of `(car)` and `(cdr)`, can be defined as follows:
```scm
(define vector-car (<-lambda (#(car : _)) car))
(define vector-cdr (<-lambda (#(_ : cdr)) cdr))
(vector-car '#(1 2 3))                ===> 1
(vector-cdr '#(1 2 3))                ===> #(2 3)
```
And we can, of course, go deeper:
```scm
(define vector-cadr (<-lambda (#(_ x : _)) x))
(vector-cadr '#(1 2 3 4 5))                ===> 2
```
Or even take it to the other end:
```scm
(define vector-rdac (<-lambda (#(: _ : x _)) x))
(vector-rdac '#(1 2 3 4 5))                ===> 4
```
### 8. Constructors

auxiliary syntax: `(vector <content>)`

auxiliary syntax: `(string <content>)`

auxiliary syntax: `(list <content>)`

Constructor patterns, depending on the root keyword used, match either vectors, strings or lists. The shape of `<content>` is not dependent on the type matched.

`<content>` is one of:
* `<pattern>...`
* `<pattern1> <pattern2>... . <pattern_t>`
* `<pattern1>... : <pattern_m>`
* `<pattern_l1>... : <pattern_m> : <pattern_r1>...`

Colon shapes work identically to those in vector patterns.

The dot-form `<pattern1> <pattern2>... . <pattern_t>` is identical to `<pattern1> <pattern2>... : <pattern_t>`.

Constructor pattern `(vector <content>)` is identical to vector pattern `#(<content>)`, when `<content>` is in colon shape, or of fixed length (no colons and no dot). When `<content>` is in dot shape, constructor pattern is identical with vector pattern after replacing the dot with colon.

Constructor patterns `(string <content>)` and `(list <content>)` work in the same way, but with strings as vector-like sequences of characters, and lists as vector-like (i.e. indexed via `(list-ref)`) sequences of objects. See [Performace Considerations](#performace-considerations) below.

Constructor pattern `(list <pattern>...)` is identical to list pattern `(<pattern>...)`. Constructor pattern `(list <pattern1> <pattern2>... . <pattern_t>)` is identical to improper list pattern `(<pattern1> <pattern2>... . <pattern_t>)`.

#### Example:
```scm
(<-let (((list a b : c : d e) '(1 2 3 4 5 6 7))) (show a #\: b #\: c #\: d #\: e))                 ===> "1:2:(3 4 5):6:7"
(<-let (((string #\H #\e #\l c1 c2 _ _ c3 . _) "Hello, world!")) (string c1 c2 c3))                ===> "low"
```
#### Performance Considerations
R5RS does not impose any expectations on the algorithmical complexity of operations defined, unless you count the remark that vector operations _can_ work faster than those working with lists. Assuming simple naive implementation has `(length)` and `(list-ref)` work in O(n), `(string-length)`, `(string-ref)`, `(vector-length)` and `(vector-ref)` in O(1), `(substring)` in O(n), we can deduce the following complexities for matching constructor pattern of m subpatterns against container of N elements:
|pattern       |estimated complexity|
|--------------|--------------------|
|`(list ...) ` |O(m * N)            |
|`(vector ...)`|O(m + N), O(m) in case of no colon/dot or of wildcard middle (something like `(vector a b c : _ : d e f)`)|
|`(string ...)`|O(m + N), O(m) in case of no colon/dot or of wildcard middle (something like `(string a b c : _ : d e f)`)|
### 9. Escapes
auxiliary syntax: `(>- <expr>)`

Escape pattern is a syntactic list of the form `(>- <expr>)`. It is atomic pattern that matches objects that are `(equal?)` to the value of `<expr>`.
If `<expr>` refers to pattern variables, they should be defined in the current environment, and then their outer bindings are used.

#### Example:
```scm
(define a 42)
(<-let (((>- (+ a 33)) 75)) a)                           ===> 42
(<-if-let (((>- a) 33)) 'match)'no-match)                ===> no-match
```
### 10. Guarded patterns

auxiliary syntax: `(<pattern> ? <expr>)`

auxiliary syntax: `(<pattern1> <pattern2>... ? <expr>)`

Guard is any expression after the question mark. The guarded `<pattern>` matches an expression iff the `<pattern>` matches this expression and guard evaluates to true for this match, i.e. with all pattern variables bound or assigned to corresponding (sub...)expressions, if any. In pattern-matching syntaxes that create new bindings for pattern variables (e.g. `(<-let)`) guard can refer to all the pattern variables in the whole root pattern to the left of question mark, not only on this level of parentheses. As a convenience, guard can end the list pattern: `(<pattern1> <pattern2>... <pattern_n-1> <pattern_n> ? <expr>)` is the same as `(<pattern1> <pattern2>... <pattern_n-1> . (<pattern_n> ? <expr>))`.

#### Example:
```scm
(<-if-let (((a (b ? (< a b)) c (d ? (= (+ a b c) d))) '(1 2 3 6))) (show a b c d) 'no-match)                                 ===> "1236"
(<-if-let (((a (b ? (< a b)) c (d . _) ? (= (+ a b c) d)) '(1 2 3 6))) (show a b c d) 'no-match)                             ===> "1236"
(<-if-let (((a (b ? (< a b)) c (d ? (= (+ a b c) d))) '(3 2 1 6))) (show a b c d) 'less-a-b-guard-failed)                    ===> less-a-b-guard-failed
(<-if-let (((a (b ? (< a b)) c (d ? (= (+ a b c) d))) '(1 2 3 7))) (show a b c d) 'd-equals-sum-of-a-b-c-guard-failed)       ===> d-equals-sum-of-a-b-c-guard-failed
```
## II. Syntaxes
Objects are matched against patterns in various pattern-matching syntaxes. The following syntaxes are defined: `(<-!)`, `(<-let)`, `(<-let*)`, `(<-if-let)`, `(<-if-let*)`, `(<-case)`, `(<-lambda)`, `(<-case-lambda)`, `(<-equal?)`. Their feature sets can be outlined briefly in this table:

|form                                               |creates bindings|assigns variables|           value if matches           |value if no match|
|---------------------------------------------------|----------------------------------|--------------------------------------|-----------------|
|`(<-! <pattern> <expr>)`                           |                |         +       |                 `#t`                 |       `#f`      |
|`(<-let ((<pattern> <expr>)...) <expr>...)`        |        +       |         +       |             last `<expr>`            |   unspecified   |
|`(<-let* ((<pattern> <expr>)...) <expr>...)`       |        +       |         +       |             last `<expr>`            |   unspecified   |
|`(<-if-let ((<pattern> <expr>)...) <then> <else>)` |        +       |         +       |               `<then>`               |     `<else>`    |
|`(<-if-let* ((<pattern> <expr>)...) <then> <else>)`|        +       |         +       |               `<then>`               |     `<else>`    |
|`(<-case <expr> <clause1> <clause2>...)`           |        +       |         +       |last `<expr>` in `<clause>`'s `<body>`|   unspecified   |
|`(<-lambda <pattern-formals> <body>)`              |        +       |         +       |      last `<expr>` in `<body>`       |      error      |
|`(<-case-lambda (<pattern-formals> <body>)...)`    |        +       |         +       |last `<expr>` in `<clause>`'s `<body>`|      error      |
|`(<-equal? <pattern> <expr>)`                      |                |                 |                 `#t`                 |       `#f`      |

Column meanings are:

creates bindings — '+' indicates that this syntax creates a fresh local binding for every pattern variable; otherwise pattern variables should all be defined in the current environment (and it is an error if they are not);

assigns variables — '+' indicates that this syntax assigns values of (sub...)expressions matched to the pattern variables. Only (<-equal?) does not do so, in other syntaxes variable is an irrefutable (sub...)pattern;

value if matches — what the syntax returns when all expressions match corresponding patterns;

value if no match — what the syntax returns or what occurs, when an expression does not match its pattern.

The syntaxes are discussed in more detail below.
### 1. `(<-!)`
library syntax: `(<-! <pattern> <expr>)`

This is the pattern-matching version of `(set!)`. All the pattern variables should be defined in the current environment, no fresh bindings are created. If `<expr>` matches `<pattern>`, pattern variables are assigned the corresponding (sub...)expressions and `#t` is returned. If `<expr>` does not match `<pattern>`, `#f` is returned, and pattern variables are left intact.

#### Example:
```scm
(define a #f)
(<-! (a #f #\H) 42)                                                              ===> #f
a                                                                                ===> #f
(<-! (a #f #\H) (list 42 (> 1 2) (string-ref "Hello, world!" 0)))                ===> #t
a                                                                                ===> 42
```
### 2. `(<-let)`

library syntax: `(<-let <pattern-bindings> <expr1> <expr2>...)`

`<pattern-bindings>` have form `((<pattern> <init>)...)`.

If all `<init>`s match corresponding `<patterns>`, pattern variables are bound in the sense of (let) to fresh locations, locations are populated with corresponding (sub...) expressions, `<expr>`s are evaluated in orders, and the values of the last `<expr>` are returned. When an `<init>` does not match its `<pattern>`, the return value is unspecified. Pattern variables referred to in `<init>`s should be defined in the current environment.

#### Example:
```scm
(define a 42)
(<-let ((a 84) ((_ . x) (list 0 a))) (show x #\: a))                ===> "(42):84"
```
### 3. `(<-let*)`

library syntax: `(<-let* <pattern-bindings> <expr1> <expr2>...)`

If all `<init>`s match corresponding `<patterns>`, pattern variables are bound in the sense of `(let*)` to fresh locations, locations are populated with corresponding (sub...) expressions, `<expr>`s are evaluated in orders, and the values of the last `<expr>` are returned. When an `<init>` does not match its `<pattern>`, the return value is unspecified.

Here's what "in the sense of `(let*)`" means. The functioning of `(<-let*)` can be approximately pictured as:
```scm
(<-let* ((<pattern1> <init1>) (<pattern2> <init2>) (<pattern3> <init3>)...) <expr1> <expr2>...) --->
(<-let ((<pattern1> <init1>)) (<-let ((<pattern2> <init2>)) (... <expr1> <expr2>...) ...))
```
That is, an `<init>` can refer to pattern variables defined in earlier `<pattern>`s.

#### Example:
```scm
(define a 42)
(<-let* ((b a) ((_ . a) (list 0 b)) (x (length a))) (show x #\: a))                ===> "1:(42)"
```
### 4. `(<-if-let)`

library syntax: `(<-if-let <pattern-bindings> <then> <else>)`

`<then>` and `<else>` are any expressions. If all `<init>`s match corresponding `<pattern>`s, pattern variables are bound in the sense of `(let)` to fresh locations, locations are populated with corresponding (sub...)expressions, and the values of `<then>` are returned. When an `<init>` does not match its `<pattern>`, the values of `<else>` are returned.

Note that if both `<then>` and `<else>` refer to the same identifier found in pattern, this references have different meanings: in `<then>` it is pattern variable, and in `<else>`, since no match exists, it is an identifier that should be bound in the current environment. Pattern variables referred to in `<init>`s should be defined in the current environment.

#### Example:
```scm
(define a 42)
(<-if-let ((a 84)) "a is 84" (show "a is " a ", not 84"))                                                  ===> "a is 84"
(<-if-let ((84 a)) "84 is a" (show "a is " a ", not 84"))                                                  ===> "a is 42, not 84"
(<-if-let (((a . _) 84)) "a is 84" (show "a is " a ", not 84"))                                            ===> "a is 42, not 84"
(<-if-let (((a 42) (list "Hello!" a)) ((_ . x) (list 0 1 a))) (show a " and " x) "unknown")                ===> "Hello! and (1 42)"
```
### 5. `(<-if-let*)`

library syntax: `(<-if-let* <pattern-bindings> <then> <else>)`

`<then>` and `<else>` are any expressions. If all `<init>`s match corresponding `<pattern>`s, pattern variables are bound in the sense of `(let*)` to fresh locations, locations are populated with corresponding (sub...)expressions, and the values of `<then>` are returned. When an `<init>` does not match its `<pattern>`, the values of `<else>` are returned.

Here's what "in the sense of `(let*)`" means. The functioning of `(<-if-let*)` can be approximately pictured as:
```scm
(<-if-let* ((<pattern1> <init1>) (<pattern2> <init2>) (<pattern3> <init3>)...) <expr1> <expr2>...) --->
(<-if-let ((<pattern1> <init1>)) (<-let ((<pattern2> <init2>)) (... <expr1> <expr2>...) ...))
```
Note that if both `<then>` and `<else>` refer to the same identifier found in pattern, this references have different meanings: in `<then>` it is pattern variable, and in `<else>`, since no match exists, it is an identifier that should be bound in the current environment. Pattern variables referred to in `<init>`s should be defined in the current environment.

#### Example:
```scm
(define a 42)
(<-if-let* (((list b) (list a)) ((a ? (and (= a 43) (= b 42))) (+ b 1))) (show a #\: b) "No match")                ===> "43:42"
(<-if-let* (((list b) a) (a (+ b 1))) (show a #\: b) "No match")                                                   ===> "No match"
```
### 6. `(<-case)`

library syntax: `(<-case <key> <clause>...)`

`<key>` is any expression, and each `<clause>` should be of form
```scm
((<pattern>...) <expr>...)
```
If `<key>` matches a `<pattern>`, pattern variables are bound to fresh locations, locations are populated with corresponding (sub...)expressions and `<expr>`s are evaluated, in order, and the values of the last `<expr>` are the return values of `(<-case)` expression. Note, that `<pattern>`s of the same `<clause>` can have different sets of pattern variables, but nevertheless, if a `<clause>` matches all the variables the `<expr>`s refer to should be either bound by pattern-matching or in the current environment.

The last `<clause>` can be an else clause:
```scm
(else <expr>...)
```
It is identical to
```scm
((_) <expr>...)
```
and thus is prone to be deprecated in the favor of the latter. The even more useful would be a variable pattern: equally irrefutable, it allows to refer to the whole `<key>` object when none of the previous `<pattern>`s matched:
```scm
((default) (println "Got an object: " default))
```
When none of the `<pattern>`s match and there is no else clause, the value of `(<-case)` expression is unspecified.

#### Example:
```scm
(define (gen-car container)
  (<-case container
    (((x . _) #(x : _) (string x : _)) x)
    ((x) (show x " does not have a car field"))
    ((_) "This clause is never reached")
    (else "This clause is even more never reached")))

(gen-car '(1 2 . 3))                     ===> 1
(gen-car '#(42 43 44))                   ===> 42
(gen-car "Hello, world!")                ===> #\H
(gen-car '())                            ===> "() does not have a car field"
(gen-car 42)                             ===> "42 does not have a car field"
```
### 7. `(<-lambda)`

library syntax: `(<-lambda <pattern-formals> <body>)`

`<pattern-formals>` is a pattern that matches a list, that is either wildcard, or a variable, or list pattern, or list constructor pattern. This form evaluates to a procedure that, whenever called, matches the list of actual arguments against `<pattern-formals>` and, if there's a match, binds pattern variables to fresh locations, populates those locations with corresponding (sub...)expressions and executes `<body>`. The values or the last expression in `<body>` are the return values of the procedure.

It is an error if actual arguments list does not match `<pattern-formals>`.

Note: every `(lambda)` expression is easily converted to `(<-lambda)` by prepending `<-` to the keyword `lambda`.

#### Example:
```scm
(define r-squared (<-lambda (#(x y z)) (+ (* x x) (* y y) (* z z))))
(define hypotenuse-squared (<-lambda (a b) (+ (r-squared a) (r-squared b))))
(define here-list (<-lambda l l))
(define ignore-list (<-lambda _ '()))
(define repeat (<-lambda (list n : fragment) (flat-map ($0 fragment) (seq-list 0 n))))

(r-squared (vector 1 2 3))                ===> 14
(here-list 1 2 3)                         ===> (1 2 3)
(ignore-list 1 2 3)                       ===> ()
(repeat 3 42 43 44)                       ===> (42 43 44 42 43 44 42 43 44)
(hypotenuse-squared 'x 'y)                ===> <execution terminated>
|(<-lambda): actual arguments do not match formals|
```
In the last case, when `(hypotenuse-squared)` tried to call `(r-squared)` passing the symbol `x` (or `y`, the order of evaluation of the arguments to `(+)` is undefined) as actual argument, it did not match with vector pattern for the only formal argument in the definition of `(r-squared)`.
### 8. (<-case-lambda)

library syntax: `(<-case-lambda <clause>...)`

Each `<clause>` is of form `(<pattern-formals> <body>)`. Each `<pattern-formals>` is a pattern that matches a list, that is either wildcard, or a variable, or list pattern, or list constructor pattern. This form evaluates to a procedure that, whenever called, matches the list of actual arguments against every `<pattern-formals>`, left-to-right, and, if there's a match, binds pattern variables to fresh locations, populates those locations with corresponding (sub...)expressions and executes `<body>` of this `<clause>`. The values or the last expression in `<body>` are the return values of the procedure.
It is an error if actual arguments list does not match `<pattern-formals>` of any clause.

#### Example:
```scm
(define improper-length
  (<-case-lambda
    ((#(_ : tail)) (+ 1 (improper-length tail)))
    (((_ . tail)) (+ 1 (improper-length tail)))
    (((string _ : tail)) (+ 1 (improper-length tail)))
    (_ 0)))

(improper-length '(1 2 3))                             ===> 3
(improper-length '#(41 42 43))                         ===> 3
(improper-length "Hello!")                             ===> 6
(improper-length '())                                  ===> 0
(improper-length '(1 2 . 3)                            ===> 2
(improper-length 42)                                   ===> 0
(improper-length '(1 2 3) '#(41 42 43))                ===> <execution terminated>
```
The last clause of the procedure actually accepts any number of arguments and immediately returns 0, as we see in the last call. To make the procedure strictly unary, the last clause should look like this:
```scm
    ((_) 0)))
```
and then, the last call would terminate with the error:
```scm
(improper-length '(1 2 3) '#(41 42 43))                ===> <execution terminated>
|(<-case-lambda): actual arguments do not match formals|
```
### 9. (<-equal?)

library syntax: `(<-equal? <pattern> <expr>)`

Returns `#t` if `<expr>` matches the `<pattern>`, and `#f` otherwise.
This is the only form that does not assign values to pattern variables; instead, all pattern variables should be defined in the current environment and their actual values are used in the matching as if they were literals. So, to use a variable's value in the pattern you don't need to escape it: `x` is the same as `(>- x)`.

#### Example:
One simple use is test for equality:
```scm
(define a 42)
(<-equal? a 42)                                            ===> #t
(define b "Hello!") (define c '(3.14159))
(<-equal? (a b . c) '(42 "Hello!" 3.14159))                ===> #t
```
Then, using synonym patterns, we can check for equality of several objects at once:
```scm
(define a #f) (define b #f) (define c #f) (define d #f)
(<-equal? (a @ (b @ c)) d)                                 ===> #t
(<-equal? (a @ (b @ (c @ d))) 42)                          ===> #f
```
`(<-equal?)` can serve as type predicate:
```scm
(define (is-vector? v) (<-equal? #(: _ :) v))
(is-vector? '#(1 2))                                       ===> #t
(is-vector? '#())                                          ===> #t
(is-vector? '(1 2))                                        ===> #f

(define (is-pair? p) (<-equal? (_ . _) p))
(is-pair? (cons 1 2))                                      ===> #t
(is-pair? '())                                             ===> #f
```
And of course, you can use it to quickly check the structure of objects:
```scm
(define a '(1 2 3))
(define b "Hello!")
```
This test checks, whether a is a list of three elements:
```scm
(<-equal? (_ _ _) a)                                       ===> #t
```
This test checks, whether b is a string of at least four characters:
```scm
(<-equal? (string _ _ _ _ : _) b)                          ===> #t
```
### 10. for-comprehension
The for-comprehension forms, `(for)` and `(do-for)`, use pattern-matching generators `(<pattern> <- list)`, `(<-let)`, `(<-let*)`, `(<-if-let)` and `(<-if-let*)`. Their semantics is close to that of namesake pattern-matching forms. See [README.comprehension](README.comprehension.md) for more.

#### Example:
```scp
(for (((odd even) <- '((1 2) (3 4) (5 6))) (<-if-let ((>- (+ odd even)) 7))) (list even odd))                ===> ((4 3))
```
