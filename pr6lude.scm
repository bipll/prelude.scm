(library (prelude.scm (0 1))
  (export
    if-identifier variable? rename same-name where
    curried-lambda arity curry error 
    <-! <-case <-if-let <-if-let* <-let <-let* <-case-lambda <-lambda <-equal?
    do-for for 
    all? any? concat drop-until drop-while drop fold-left fold-right foldl foldr init last init-last list-head
    take seq-list take-until take-while filter flat-map for-each-if for-each-while for-each-if-while for-each-while-if
    map-if map-while map-if-while map-while-if 
    obj->string show source subvector
    $% $* $+ $0 $@ id
    fprint fprintln print println report)
  (import (rnrs base (6)))
  (define (print . args) (for-each display args))
  (define (fprint port . args) (for-each (lambda (obj) (display obj port)) args))
  (define (println . args) (for-each display args) (newline))
  (define (fprintln port . args) (for-each (lambda (obj) (display obj port)) args) (newline port))
  (define-syntax report
    (syntax-rules ()
		  ((_ arg) (report "		===> " arg))
		  ((_ arrow args ...) (println (source 'args) ... arrow (source args) ...))))

  (define-syntax unspecified (syntax-rules () ((unspecified) (if #f #f))))

  (define (obj->string obj)
    (cond
      ((boolean? obj) (if obj "#t" "#f"))
      ((symbol? obj) (symbol->string obj))
      ((char? obj) (make-string 1 obj))
      ((vector? obj) (string-append "#" (obj->string (vector->list obj))))
      ((procedure? obj) "#<PROCEDURE>")
      ((pair? obj)
       (let build ((ret-val (obj->string (car obj))) (next (cdr obj)))
	 (cond
	   ((pair? next) (build (string-append ret-val " " (obj->string (car next))) (cdr next)))
	   ((null? next) (string-append "(" ret-val ")"))
	   (else (string-append "(" ret-val " . " (obj->string next) ")")))))
      ((number? obj) (number->string obj))
      ((port? obj) "#<PORT>")
      ((null? obj) "()")
      (else obj)))

  (define (show . objs) (apply string-append (map obj->string objs)))

  (define (source obj)
    (cond
      ((boolean? obj) (if obj "#t" "#f"))
      ((symbol? obj) (symbol->string obj))
      ((char? obj) (cond
		     ((char=? obj #\space) "#\\space")
		     ((char=? obj #\newline) "#\\newline")
		     (else (string-append "#\\" (make-string 1 obj)))))
      ((vector? obj) (string-append "#" (source (vector->list obj))))
      ((procedure? obj) "#<PROCEDURE>")
      ((pair? obj)
       (let build ((ret-val (source (car obj))) (next (cdr obj)))
	 (cond
	   ((pair? next) (build (string-append ret-val " " (source (car next))) (cdr next)))
	   ((null? next) (string-append "(" ret-val ")"))
	   (else (string-append "(" ret-val " . " (source next) ")")))))
      ((number? obj) (number->string obj))
      ((port? obj) "#<PORT>")
      ((null? obj) "()")
      (else
	(string-append "\""
		       (apply string-append
			      (map (lambda (c) (if (or (char=? c #\\) (char=? c #\")) (string #\\ c) (make-string 1 c)))
				   (string->list obj)))
		       "\""))))

  (define (error . msgs) (apply println msgs) (println (/ 1 0)))

  (define-syntax where (syntax-rules () ((where expr bindings ...) (let (bindings ...) expr))))

  (define-syntax variable? (syntax-rules () ((_ x) (symbol? 'x))))

  (define-syntax rename
    (syntax-rules ()
		  ((_ ((n v) ...) body)
		   (let-syntax ((substitute (syntax-rules () ((_ n ...) body))))
		     (substitute v ...)))))

  (define-syntax same-name (syntax-rules () ((_ mappings body) body)))

  (define (id x) x)

  (define ($0 v . vs) (if (null? vs) (lambda args v) (lambda args (apply values v vs))))

  (define ($@ f . args) (apply f args))

  (define ($+ f . args) (lambda args1 (apply f (append args args1))))

  (define ($% f) (lambda (x y . zs) (apply f y x zs)))

  (define ($* . procs)
    (if (null? procs) values
      (let* ((scorp (reverse procs)) (fst (car scorp)) (oth (cdr scorp)))
	(if (null? oth) fst
	  (lambda args
	    (call-with-values
	      (lambda () (apply fst args))
	      (let chain ((next (car oth)) (rest (cdr oth)))
		(if (null? rest) next
		  (lambda args (call-with-values (lambda () (apply next args)) (chain (car rest) (cdr rest))))))))))))

  (define (seq-list b e . s)
    (let ((seq (lambda (b e s)
		 (cond
		   ((positive? s) (let build ((ret-val '()) (b b)) (if (>= b e) (reverse ret-val) (build (cons b ret-val) (+ b s)))))
		   ((negative? s) (let build ((ret-val '()) (b b)) (if (<= b e) (reverse ret-val) (build (cons b ret-val) (+ b s)))))
		   ((zero? s) (error "(seq-list): zero step would yield infinite list"))))))
      (cond
	((null? s) (seq b e 1))
	((null? (cdr s)) (seq b e (car s)))
	(else (error "(seq-list): too many arguments")))))

  (define (all? p? l . ls)
    (if (null? ls) (let test ((l l)) (or (null? l) (and (p? (car l)) (test (cdr l)))))
      (let test ((l l) (ls ls)) (or (null? l) (and (apply p? (car l) (map car ls)) (test (cdr l) (map cdr ls)))))))

  (define (any? p? l . ls)
    (if (null? ls) (let test ((l l)) (and (pair? l) (or (and (p? (car l)) #t) (test (cdr l)))))
      (let test ((l l) (ls ls)) (and (pair? l) (or (and (apply p? (car l) (map car ls)) #t) (test (cdr l) (map cdr ls)))))))

  (define (filter p? l . ls)
    (if (null? ls) (apply append (map (lambda (a) (if (p? a) (list a) '())) l))
      (let* ((flags (apply map p? l ls)) (select (lambda (l) (apply append (map (lambda (f x) (if f (list x) '())) flags l)))))
	(apply values (select l) (map select ls)))))

  (define (map-if f p? l . ls)
    (if (null? ls) (apply append (map (lambda (a) (if (p? a) (list (f a)) '())) l))
      (apply append (apply map (lambda (a . args) (if (apply p? a args) (list (apply f a args)) '())) l ls))))

  (define (map-while f p? l . ls)
    (if (null? ls)
      (let map-while ((ret-val '()) (l l))
	(if (null? l) (reverse ret-val)
	  (let ((head (car l)))
	    (if (p? head) (map-while (cons (f head) ret-val) (cdr l)) (reverse ret-val)))))
      (let map-while ((ret-val '()) (l l) (ls ls))
	(if (null? l) (reverse ret-val)
	  (let ((head (car l)) (heads (map car ls)))
	    (if (apply p? head heads) (map-while (cons (apply f head heads) ret-val) (cdr l) (map cdr ls)) (reverse ret-val)))))))

  (define (map-if-while f i? w? l . ls)
    (if (null? ls)
      (let map-iw ((ret-val '()) (l l))
	(if (null? l) (reverse ret-val)
	  (let ((head (car l)))
	    (if (w? head) (if (i? head) (map-iw (cons (f head) ret-val) (cdr l)) (map-iw ret-val (cdr l))) (reverse ret-val)))))
      (let map-iw ((ret-val '()) (l l) (ls ls))
	(if (null? l) (reverse ret-val)
	  (let ((head (car l)) (heads (map car ls)))
	    (if (apply w? head heads)
	      (if (apply i? head heads)
		(map-iw (cons (apply f head heads) ret-val) (cdr l) (map cdr ls))
		(map-iw ret-val (cdr l) (map cdr ls)))
	      (reverse ret-val)))))))

  (define (map-while-if f w? i? l . ls)
    (if (null? ls)
      (let map-wi ((ret-val '()) (l l))
	(if (null? l) (reverse ret-val)
	  (let ((head (car l)))
	    (if (i? head) (if (w? head) (map-wi (cons (f head) ret-val) (cdr l)) (reverse ret-val)) (map-wi ret-val (cdr l))))))
      (let map-wi ((ret-val '()) (l l) (ls ls))
	(if (null? l) (reverse ret-val)
	  (let ((head (car l)) (heads (map car ls)))
	    (if (apply i? head heads)
	      (if (apply w? head heads) (map-wi (cons (apply f head heads) ret-val) (cdr l) (map cdr ls)) (reverse ret-val))
	      (map-wi ret-val (cdr l) (map cdr ls))))))))

  (define (for-each-if f p? l . ls)
    (if (null? ls) (for-each (lambda (x) (if (p? x) (f x))) l)
      (apply for-each (lambda (a . as) (if (apply p? a as) (apply f a as))) l ls)))

  (define (for-each-while f p? l . ls)
    (call/cc (lambda (return)
	       (if (null? ls) (for-each (lambda (x) (if (p? x) (f x) (return (unspecified)))) l)
		 (apply for-each (lambda (a . as) (if (apply p? a as) (apply f a as) (return (unspecified)))) l ls)))))

  (define (for-each-if-while f i? w? l . ls)
    (call/cc
      (lambda (return)
	(if (null? ls) (for-each (lambda (x) (if (w? x) (if (i? x) (f x)) (return (unspecified)))) l)
	  (apply for-each (lambda (a . as) (if (apply w? a as) (if (apply i? a as) (apply f a as)) (return (unspecified)))) l ls)))))

  (define (for-each-while-if f w? i? l . ls)
    (call/cc
      (lambda (return)
	(if (null? ls) (for-each (lambda (x) (if (i? x) (if (w? x) (f x) (return (unspecified))))) l)
	  (apply for-each (lambda (a . as) (if (apply i? a as) (if (apply w? a as) (apply f a as) (return (unspecified))))) l ls)))))

  (define (take-until p? l . ls)
    (if (null? ls)
      (if (null? l) '()
	(let build ((ret-val '()) (head (car l)) (tail (cdr l)))
	  (if (p? head) (reverse ret-val)
	    (let ((next (cons head ret-val)))
	      (if (null? tail) (reverse next) (build next (car tail) (cdr tail)))))))
      (let ((empties (map ($0 '()) ls)))
	(if (null? l) (apply values '() empties)
	  (let build ((rv '()) (rvs empties) (h (car l)) (t (cdr l)) (hs (map car ls)) (ts (map cdr ls)))
	    (if (apply p? h hs) (apply values (reverse rv) (map reverse rvs))
	      (let ((n (cons h rv)) (ns (map cons hs rvs)))
		(if (null? t) (apply values (reverse n) (map reverse rvs))
		  (build n ns (car t) (cdr t) (map car ts) (map cdr ts))))))))))

  (define (take-while p? l . ls)
    (if (null? ls)
      (if (null? l) '()
	(let build ((ret-val '()) (head (car l)) (tail (cdr l)))
	  (if (p? head)
	    (let ((next (cons head ret-val)))
	      (if (null? tail) (reverse next) (build next (car tail) (cdr tail))))
	    (reverse ret-val))))
      (let ((empties (map ($0 '()) ls)))
	(if (null? l) (apply values '() empties)
	  (let build ((rv '()) (rvs empties) (l l) (ls ls))
	    (let ((h (car l)) (hs (map car ls)))
	      (if (apply p? h hs) (apply values (reverse rv) (map reverse rvs))
		(let ((t (cdr l)) (n (cons h rv)) (ns (map cons hs rvs)))
		  (if (null? t) (apply values (reverse n) (map reverse rvs))
		    (let ((ts (map cdr ls))) (build n ns (car t) (cdr t) (map cadr ts) (map cddr ts))))))))))))

  (define (drop-while p? l . ls)
    (if (null? ls) (let drop-while ((l l)) (if (and (pair? l) (p? (car l))) (drop-while (cdr l)) l))
      (let drop-while ((l l) (ls ls))
	(if (and (pair? l) (apply p? (car l) (map car ls))) (drop-while (cdr l) (map cdr ls)) (apply values l ls)))))

  (define (drop-until p? l . ls)
    (if (null? ls) (let drop-until ((p? p?) (l l)) (if (or (null? l) (p? (car l))) l (drop-until p? (cdr l))))
      (let drop-until ((l l) (ls ls))
	(if (or (null? l) (apply p? (car l) (map car ls))) (apply values l ls) (drop-until (cdr l) (map cdr ls))))))

  (define (drop k l . ls)
    (if (null? ls) (list-tail l k)
      (let take ((k k) (l l) (ls ls))
	(if (> k 3) (take (- k 4) (cddddr l) (map cddddr ls))
	  (case k
	    ((0) (apply values l ls))
	    ((1) (apply values (cdr l) (map cdr ls)))
	    ((2) (apply values (cddr l) (map cddr ls)))
	    ((3) (apply values (cdddr l) (map cdddr ls)))
	    (else (error "(drop): wrong amount " k)))))))

  (define (list-head l k)
    (if (and (integer? k) (positive? k))
      (let take ((ret-val '()) (l l) (k k))
	(cond
	  ((zero? k) (reverse ret-val))
	  ((null? l) (error "(list-head): list too short"))
	  (else (take (cons (car l) ret-val) (cdr l) (- k 1)))))
      (error "(list-head): wrong amount " k)))

  (define (take k l . ls)
    (if (and (integer? k) (positive? k))
      (if (null? ls)
	(let take ((ret-val '()) (k k) (l l))
	  (cond
	    ((zero? k) (reverse ret-val))
	    ((null? l) (error "(take): list too short"))
	    (else (take (cons (car l) ret-val) (- k 1) (cdr l)))))
	(let take ((rv '()) (rvs (map (lambda (x) '()) ls)) (k k) (l l) (ls ls))
	  (cond
	    ((zero? k) (apply values (reverse rv) (map reverse rvs)))
	    ((null? l) (error "(take): lists too short"))
	    (else (let ((c-c (lambda (l rv) (cons (car l) rv)))) (take (c-c l rv) (map c-c ls rvs) (- k 1) (cdr l) (map cdr ls)))))))
      (error "(take): wrong amount " k)))

  (define (concat l) (apply append l))

  (define (flat-map f l . ls) (apply append (apply map f l ls)))

  (define (fold-left f z l . ls)
    (if (null? ls)
      (let fold ((z z) (l l))
	(if (null? l) z (fold (f z (car l)) (cdr l))))
      (let fold ((z z) (l l) (ls ls))
	(if (null? l) z (fold (apply f z (car l) (map car ls)) (cdr l) (map cdr ls))))))

  (define (fold-right f z l . ls)
    (if (null? ls)
      (let fold ((l (reverse l)) (z z))
	(if (null? l) z (fold (cdr l) (f (car l) z))))
      (let fold ((l (reverse l)) (ls (map reverse ls)) (z z))
	(if (null? l) z (fold (cdr l) (map cdr ls) (apply f (car l) (append (map car ls) (list z))))))))

  (define (foldl f l)
    (if (null? l) (error "foldl: empty list")
      (let fold ((z (car l)) (l (cdr l)))
	(if (null? l) z (fold (f z (car l)) (cdr l))))))

  (define (foldr f l)
    (if (null? l) (error "foldr: empty list")
      (let ((l (reverse l)))
	(let fold ((l (cdr l)) (z (car l)))
	  (if (null? l) z (fold (cdr l) (f (car l) z)))))))

  (define (init l) (reverse (cdr (reverse l))))

  (define (last l) (car (reverse l)))

  (define (init-last l) (let ((rev-l (reverse l))) (values (reverse (cdr l)) (car l))))

  (define (subvector v b . e)
    (cond
      ((null? e) (let ((max (vector-length v)))
		   (if (= b max) '#()
		     (let ((ret-val (make-vector (- max b) (vector-ref v b))))
		       (unsafe-copy! v ret-val (+ b 1) max b)))))
      ((null? (cdr e)) (let ((max (car e)))
			 (cond
			   ((< b max) (let ((ret-val (make-vector (- max b) (vector-ref v b))))
					(unsafe-copy! v ret-val (+ b 1) max b)))
			   ((= b max) '#())
			   (else (error "(subvector): upper limit out of bounds")))))
      (else (error "(subvector): too many arguments"))))

  (define (unsafe-copy! src dst b e db)
    (do ((i b (+ i 1))) ((= i e) dst) (vector-set! dst (- i db) (vector-ref src i))))

  ;; Pattern buildup rules:
  ;; shape                        refutable?      semantics
  ;; _                            #f              matches everything;
  ;; ()                           #t              matches empty list;
  ;; (a ... n)                    #t              matches list of n elements, one-by-one
  ;; (a ... n . t)                #t              matches first n elements of the (improper) list, with t the tail
  ;; (name @ pat)                 #t              if pat matches, name is assigned the overall expression
  ;; (>- expr)                    #t              matches the value of expr
  ;; v                            #f              a single variable assignment
  ;; literal                      #t		matches expression equal? to the literal
  ;; pat ? guard                  #t		matches when pat matches and guard is true
  ;; #(a ... n)                   #t		matches vector of n elements, one-by-one
  ;; #(a ... n : t)        	#t		matches vector with n first elements, one-by-one, and t the `tail`
  ;; #(: i : a ... n)             #t		matches vector of n last elements, with i the prefix
  ;; #(a ... n : m : b ... k)     #t              matches vector of at least n+k elements, m being the middle subvector
  ;; (vector v0 ...)              #t		almost the same as #(v0 ...), single tail can be delimited with .
  ;; (string c0 ...)              #t              like (vector c0 ...), but matches a string
  ;; (list l0 ...)                #t              like (vector c0 ...), but matches a list

  (define-syntax <-! (syntax-rules () ((_ pattern expression) (<-build (() (<-build-!) <-identifier same-name) pattern expression))))

  (define-syntax <-let
    (syntax-rules ()
		  ((_ bindings body ...) (<-start-let let same-name ((unspecified) body ...) bindings))))
  (define-syntax <-let*
    (syntax-rules ()
		  ((_ bindings body ...) (<-start-let let* rename ((unspecified) body ...) bindings))))
  (define-syntax <-letrec
    (syntax-rules ()
		  ((_ bindings body ...) (<-start-let letrec same-name ((unspecified) body ...) bindings))))

  (define-syntax <-if-let
    (syntax-rules ()
		  ((_ bindings then else) (<-start-let let same-name (else then) bindings))))
  (define-syntax <-if-let*
    (syntax-rules ()
		  ((_ bindings then else) (<-start-let let* rename (else then) bindings))))
  (define-syntax <-if-letrec
    (syntax-rules ()
		  ((_ bindings then else) (<-start-let letrec same-name (else then) bindings))))

  (define-syntax <-equal? (syntax-rules () ((_ pat expr) (<-build (() (<-true) <-literal same-name) pat expr))))

  (define-syntax <-lambda
    (syntax-rules ()
      ((_ formals body ...)
       (lambda args
	 (<-start-let let same-name ((error "(<-lambda): actual arguments do not match formals") body ...) ((formals args)))))))

  (define-syntax <-case-lambda
    (syntax-rules ()
		  ((_ case ...) (lambda args (<-case-lambda-impl args case ...)))))

  (define-syntax <-case
    (syntax-rules (else)
		  ((_ expr ((pat pats ...) body ...) clauses ...)
		   (if-identifier expr
				  (<-start-let let same-name ((<-case expr ((pats ...) body ...) clauses ...) body ...) ((pat expr)))
				  (let ((key expr)) (<-case key ((pat pats ...) body ...) clauses ...))))
		  ((_ expr mismatch next-clause clauses ...) (<-case expr next-clause clauses ...))
		  ((_ expr (else body)) body)
		  ((_ expr (else body ...)) (begin body ...))
		  ((_ expr) (unspecified))))

  (define-syntax <-case-lambda-impl
    (syntax-rules ()
		  ((_ args) (error "(<-case-lambda): actual arguments do not match formals"))
		  ((_ args (formals body ...) cases ...)
		   (<-start-let let same-name ((<-case-lambda-impl args cases ...) body ...) ((formals args))))))

  (define-syntax <-start-let
    (syntax-rules ()
      ((_ keyword name alternatives ((p e) ps ...) fs ...) (<-start-let keyword name alternatives (ps ...) fs ... p e))
      ((_ keyword name (else then ...) () fs ...)
       (call/cc (lambda (actually) (or (<-build (() (<-build-let keyword actually then ...) <-identifier name) fs ...) else))))))

  (define-syntax <-build
    (syntax-rules
      (@ : _ >- ? string vector list)
      ; Terminal case
      ((_ (ms (syntax forms ...) ds ...)) (syntax ms forms ...))
      ; (Purportedly) random access-containers are treated the same
      ((_ (ms syn atom name) #(v ...) e stack ...)
       (let ((pv (name ms e)))
	 (and (vector? pv) (<-vector (vector-ref <-subvector 0 (vector-length pv)) (ms syn atom name) (v ...) pv stack ...))))
      ;((_ (ms syn atom name) (vector v ...) e stack ...)
      ;(let ((pv (name ms e)))
      ;(and (vector? pv) (<-vector (vector-ref <-subvector 0 (vector-length pv)) (ms syn atom name) (v ...) pv stack ...))))
      ;((_ (ms syn atom name) (string v ...) e stack ...)
      ;(let ((pv (name ms e)))
      ;(and (string? pv) (<-vector (string-ref substring 0 (string-length pv)) (ms syn atom name) (v ...) pv stack ...))))
      ;((_ (ms syn atom name) (list v ...) e stack ...)
      ;(let ((pv (name ms e))) (and (list? pv) (<-vector (list-ref <-sublist 0 (length pv)) (ms syn atom name) (v ...) pv stack ...))))
      ((_ (ms syn atom name) (vector . vs) e stack ...)
       (let ((pv (name ms e)))
	 (and (vector? pv) (<-vector (vector-ref <-subvector 0 (vector-length pv)) (ms syn atom name) vs pv stack ...))))
      ((_ (ms syn atom name) (string . vs) e stack ...)
       (let ((pv (name ms e)))
	 (and (string? pv) (<-vector (string-ref substring 0 (string-length pv)) (ms syn atom name) vs pv stack ...))))
      ((_ (ms syn atom name) (list . vs) e stack ...)
       (let ((pv (name ms e))) (and (list? pv) (<-vector (list-ref <-sublist 0 (length pv)) (ms syn atom name) vs pv stack ...))))
      ; Guards
      ((_ data (p ? g) e fs ...) (<-build data p e ? g fs ...))
      ((_ (ms syn ...) ? g stack ...) (and (rename ms g) (<-build (ms syn ...) stack ...)))
      ; Cases handling
      ((_ data (_ @ _) e stack ...) (<-build data stack ...))
      ((_ data (p @ _) stack ...) (<-build data p stack ...))
      ((_ data (_ @ p) stack ...) (<-build data p stack ...))
      ((_ (ms syn atom name) (p @ ps) e stack ...) (let ((pv (name ms e))) (<-build (ms syn atom name) p pv ps pv stack ...)))
      ((_ (ms syn atom name) (p @ . ps) e stack ...) (let ((pv (name ms e))) (<-build (ms syn atom name) p pv ps pv stack ...)))
      ((_ (ms syn atom name) (>- val) e stack ...) (and (equal? val (name ms e)) (<-build (ms syn atom name) stack ...)))
      ((_ (ms syn atom name) (p . ps) e stack ...)
       (let ((ev (name ms e))) (and (pair? ev) (<-build (ms syn atom name) p (car ev) ps (cdr ev) stack ...))))
      ((_ (ms syn atom name) () e stack ...) (and (null? (name ms e)) (<-build (ms syn atom name) stack ...)))
      ((_ data _ e stack ...) (<-build data stack ...))
      ((_ (ms syn atom name) p e stack ...) (let ((pv (name ms e))) (atom (ms syn atom name) p pv stack ...)))))

  (define-syntax <-literal (syntax-rules () ((_ data p pv stack ...) (and (equal? p pv) (<-build data stack ...)))))
  (define-syntax <-identifier
    (syntax-rules ()
		  ((_ ((m ...) ds ...) p pv stack ...)
		   (if-identifier p (<-build ((m ... (p pv)) ds ...) stack ...) (<-literal ((m ...) ds ...) p pv stack ...)))))

  (define-syntax <-build-!
    (syntax-rules ()
		  ((_ ()) #t)
		  ((_ ((id v) ...)) (begin (set! id v) ... #t))))

  (define-syntax <-true (syntax-rules () ((_ ms) #t)))

  (define-syntax <-build-let
    (syntax-rules ()
		  ((_ () keyword actually body ...) (actually (begin body ...)))
		  ((_ bindings keyword actually body ...) (actually (keyword bindings body ...)))))

  ; FIXME: <-vector is a nightmare
  (define-syntax <-vector
    (syntax-rules
      (: @)
      ((_ (ref sub beg end) data (: vs) e stack ...) (and (<= beg end) (<-build data vs (sub e beg end) stack ...)))
      ((_ (ref sub beg end) data (: vs :) e stack ...)
       (and (<= beg end) (<-build data vs (sub e beg end) stack ...)))
      ((_ interface data (: vs : x ...) e stack ...) (<-vector interface data (: vs () : x ...) e stack ...))
      ((_ interface data (: vs (v ...) : x xs ...) e stack ...) (<-vector interface data (: vs (x v ...) : xs ...) e stack ...))
      ((_ (ref sub beg end) data (: vs (x xs ...) :) e stack ...)
       (<-vector (ref sub beg (- end 1)) data (: vs (xs ...) :) e x (ref e (- end 1)) stack ...))
      ((_ (ref sub beg end) data (: vs () :) e stack ...)
       (and (<= beg end) (<-build data vs (sub e beg end) stack ...)))
      ((_ (ref sub beg end) data (v @ vs) e stack ...)
       (and (<= beg end) (let ((fv (sub e beg end))) (<-vector (ref sub beg end) data vs e v fv stack ...))))
      ((_ (ref sub beg end) data (v @ . vs) e stack ...)
       (and (<= beg end) (let ((fv (sub e beg end))) (<-vector (ref sub beg end) data vs e v fv stack ...))))
      ((_ (ref sub beg end) data (v . vs) e stack ...) (<-vector (ref sub (+ beg 1) end) data vs e v (ref e beg) stack ...))
      ((_ (ref sub beg end) data () e stack ...) (and (= beg end) (<-build data stack ...)))
      ((_ (ref sub beg end) data vs e stack ...)
       (and (<= beg end) (let ((fv (sub e beg end))) (<-build data vs (sub e beg end) stack ...))))))

  (define-syntax if-identifier
    (syntax-rules ()
		  ((_ (x . y) then else) else)
		  ((_ #(x ...) then else) else)
		  ((_ l then else)
		   (let-syntax
		     ((isyn
			(syntax-rules ()
				      ((_ l t f) t)
				      ((_ x t f) f))))
		     (isyn atom then else)))))

  (define (<-sublist l b e)
    (let ((suffix (list-tail l b)))
      (let but-last ((suffix suffix) (b b) (built '()))
	(if (= b e) (reverse built) (but-last (cdr suffix) (+ b 1) (cons (car suffix) built))))))

  (define (<-subvector v b e)
    (if (= b e) '#() (let ((ret-val (make-vector (- e b) (vector-ref v b)))) (unsafe-copy! v ret-val (+ b 1) e b) ret-val)))

  (define (curry proc arity)
    (lambda args (let ((na (length args))) (if (< na arity) (curry (apply $+ proc args) (- arity na)) (apply proc args)))))
  (define-syntax arity
    (syntax-rules ()
		  ((_ (a)) 1)
		  ((_ (a . as)) (+ 1 (arity as)))
		  ((_ a) 0)))
  (define-syntax curried-lambda
    (syntax-rules
      ()
      ((_ args body ...) (curry (lambda args body ...) (arity args)))))

  (define-syntax for
    (syntax-rules (<- ? ?? let let* letrec <-let <-let* <-if-let <-if-let*)
		  ((for ((let bindings ...)) expr ...) (list (let (bindings ...) expr ...)))
		  ((for ((let bindings ...) bgs ...) expr ...) (let (bindings ...) (for (bgs ...) expr ...)))
		  ((for ((let* bindings ...)) expr ...) (list (let* (bindings ...) expr ...)))
		  ((for ((let* bindings ...) bgs ...) expr ...) (let* (bindings ...) (for (bgs ...) expr ...)))
		  ((for ((letrec bindings ...)) expr ...) (list (letrec (bindings ...) expr ...)))
		  ((for ((letrec bindings ...) bgs ...) expr ...) (letrec (bindings ...) (for (bgs ...) expr ...)))
		  ((for ((<-let bindings ...)) expr ...) (list (<-let (bindings ...) expr ...)))
		  ((for ((<-let bindings ...) bgs ...) expr ...) (<-let (bindings ...) (for (bgs ...) expr ...)))
		  ((for ((<-let* bindings ...)) expr ...) (list (<-let* (bindings ...) expr ...)))
		  ((for ((<-let* bindings ...) bgs ...) expr ...) (<-let* (bindings ...) (for (bgs ...) expr ...)))
		  ;((for ((<-letrec bindings ...)) expr ...) (list (<-letrec (bindings ...) expr ...)))
		  ;((for ((<-letrec bindings ...) bgs ...) expr ...) (<-letrec (bindings ...) (for (bgs ...) expr ...)))
		  ((for ((<-if-let bindings ...)) expr ...) (<-if-let (bindings ...) (list (begin expr ...)) '()))
		  ((for ((<-if-let bindings ...) bgs ...) expr ...) (<-if-let (bindings ...) (for (bgs ...) expr ...) '()))
		  ((for ((<-if-let* bindings ...)) expr ...) (<-if-let* (bindings ...) (list (begin expr ...)) '()))
		  ((for ((<-if-let* bindings ...) bgs ...) expr ...) (<-if-let* (bindings ...) (for (bgs ...) expr ...) '()))
		  ;((for ((<-if-letrec bindings ...)) expr ...) (list (<-if-letrec (bindings ...) (for ((_ <- '(#f))) expr ...) '())))
		  ;((for ((<-if-letrec bindings ...) bgs ...) expr ...) (<-if-letrec (bindings ...) (for (bgs ...) expr ...) '()))
		  ((for ((x <- xs)) expr ...) (map (<-lambda (x) expr ...) xs))
		  ((for ((x <- xs ? guard ?? while ...)) expr ...)
		   (map-if-while (<-lambda (x) expr ...) (<-lambda (x) guard) (<-lambda (x) (and while ...)) xs))
		  ((for ((x <- xs ?? while ? guard ...)) expr ...)
		   (map-while-if (<-lambda (x) expr ...) (<-lambda (x) while) (<-lambda (x) (and guard ...)) xs))
		  ((for ((x <- xs ? guard ...)) expr ...) (map-if (<-lambda (x) expr ...) (<-lambda (x) (and guard ...)) xs))
		  ((for ((x <- xs ?? while ...)) expr ...) (map-while (<-lambda (x) expr ...) (<-lambda (x) (and while ...)) xs))
		  ((for ((x xs)) expr ...) (map (lambda (x) expr ...) xs))
		  ((for ((x xs ? guard ?? while ...)) expr ...)
		   (map-if-while (lambda (x) expr ...) (lambda (x) guard) (lambda (x) (and while ...)) xs))
		  ((for ((x xs ?? while ? guard ...)) expr ...)
		   (map-while-if (lambda (x) expr ...) (lambda (x) while) (lambda (x) (and guard ...)) xs))
		  ((for ((x xs ? guard ...)) expr ...) (map-if (lambda (x) expr ...) (lambda (x) (and guard ...)) xs))
		  ((for ((x xs ?? while ...)) expr ...) (map-while (lambda (x) expr ...) (lambda (x) (and while ...)) xs))
		  ((for (bg bgs ...) expr ...) (concat (for (bg) (for (bgs ...) expr ...))))))

  (define-syntax do-for
    (syntax-rules (<- ? ?? let let* letrec <-let <-let* <-if-let <-if-let*)
		  ((for ((let bindings ...)) expr ...) (let (bindings ...) expr ...))
		  ((for ((let bindings ...) bgs ...) expr ...) (let (bindings ...) (do-for (bgs ...) expr ...)))
		  ((for ((let* bindings ...)) expr ...) (let* (bindings ...) expr ...))
		  ((for ((let* bindings ...) bgs ...) expr ...) (let* (bindings ...) (do-for (bgs ...) expr ...)))
		  ((for ((letrec bindings ...)) expr ...) (letrec (bindings ...) expr ...))
		  ((for ((letrec bindings ...) bgs ...) expr ...) (letrec (bindings ...) (do-for (bgs ...) expr ...)))
		  ((for ((<-let bindings ...)) expr ...) (<-let (bindings ...) expr ...))
		  ((for ((<-let bindings ...) bgs ...) expr ...) (<-let (bindings ...) (do-for (bgs ...) expr ...)))
		  ((for ((<-let* bindings ...)) expr ...) (<-let* (bindings ...) expr ...))
		  ((for ((<-let* bindings ...) bgs ...) expr ...) (<-let* (bindings ...) (do-for (bgs ...) expr ...)))
		  ;((for ((<-letrec bindings ...)) expr ...) (<-letrec (bindings ...) expr ...))
		  ;((for ((<-letrec bindings ...) bgs ...) expr ...) (<-letrec (bindings ...) (do-for (bgs ...) expr ...)))
		  ((for ((<-if-let bindings ...)) expr ...) (<-if-let (bindings ...) (do-for ((_ <- '(#f))) expr ...) '()))
		  ((for ((<-if-let bindings ...) bgs ...) expr ...) (<-if-let (bindings ...) (do-for (bgs ...) expr ...) '()))
		  ((for ((<-if-let* bindings ...)) expr ...) (<-if-let* (bindings ...) (do-for ((_ <- '(#f))) expr ...) '()))
		  ((for ((<-if-let* bindings ...) bgs ...) expr ...) (<-if-let* (bindings ...) (do-for (bgs ...) expr ...) '()))
		  ;((for ((<-if-letrec bindings ...)) expr ...) (<-if-letrec (bindings ...) (do-for ((_ <- '(#f))) expr ...) '()))
		  ;((for ((<-if-letrec bindings ...) bgs ...) expr ...) (<-if-letrec (bindings ...) (do-for (bgs ...) expr ...) '()))
		  ((for ((x <- xs)) expr ...) (for-each (<-lambda (x) expr ...) xs))
		  ((for ((x <- xs ? guard ?? while ...)) expr ...)
		   (for-each-if-while (<-lambda (x) expr ...) (<-lambda (x) guard) (<-lambda (x) (and while ...)) xs))
		  ((for ((x <- xs ?? while ? guard ...)) expr ...)
		   (for-each-while-if (<-lambda (x) expr ...) (<-lambda (x) while) (<-lambda (x) (and guard ...)) xs))
		  ((for ((x <- xs ? guard ...)) expr ...) (for-each-if (<-lambda (x) expr ...) (<-lambda (x) (and guard ...)) xs))
		  ((for ((x <- xs ?? while ...)) expr ...) (for-each-while (<-lambda (x) expr ...) (<-lambda (x) (and while ...)) xs))
		  ((for ((x xs)) expr ...) (for-each (lambda (x) expr ...) xs))
		  ((for ((x xs ? guard ?? while ...)) expr ...)
		   (for-each-if-while (lambda (x) expr ...) (lambda (x) guard) (lambda (x) (and while ...)) xs))
		  ((for ((x xs ?? while ? guard ...)) expr ...)
		   (for-each-while-if (lambda (x) expr ...) (lambda (x) while) (lambda (x) (and guard ...)) xs))
		  ((for ((x xs ? guard ...)) expr ...) (for-each-if (lambda (x) expr ...) (lambda (x) (and guard ...)) xs))
		  ((for ((x xs ?? while ...)) expr ...) (for-each-while (lambda (x) expr ...) (lambda (x) (and while ...)) xs))
		  ((for (bg bgs ...) expr ...) (do-for (bg) (do-for (bgs ...) expr ...))))))
