### 4.1.4. Procedures

library syntax: `(<-case-lambda (<formals1> <body1>) (<formals2> <body2>)...)`

Every ``<formals>`` should be a list pattern; every ``<body>`` is a sequence of one or more expressions that can refer to pattern variables in corresponding `<formals>`. This form evaluates to procedure that, whenever called, matches the list of actual arguments against all `<formals>`, in order, and for the first `<formals>` that match, the corresponding `<body>` is executed, and the value of the last expression in that `<body>` is the return value of the procedure. If none of the `<formals>` match, an error is reported.

See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(define improper-length
  (<-case-lambda
    (((_ . tail)) (+ 1 (improper-length tail)))      ; tail is bound to cdr of the list argument
    (_ 0)))                                          ; default wildcard pattern matches every object
(improper-length '(1 2 3 4))                ===> 4
(improper-length '(1 2 . 3))                ===> 2   ; cddr of this list is not a list and has improper-length of 0
(improper-length '())                       ===> 0
(improper-length 42)                        ===> 0
```
--------------
library syntax: `(<-lambda <formals> <body>)`

`<formals>` should be a list pattern; `<body>` is a sequence of one or more expressions that can refer to pattern variables bound in `<formals>`. This form evaluates to procedure that, whenever called, matches the list of actual arguments against `<formals>`, executes expressions in `<body>` and returns the values returned by the last one of them. When actual arguments list does not match `<formals>`, an error is reported.

See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(define f (<-lambda _ (println "Ignore all arguments") 42))
(define g (<-lambda ((x y z . _)) (println "Some values: " x y z ", etc.") (+ x y z)))
(f 1 2 3 4)                   ===> 42
|Ignore all arguments                             |
(f)                           ===> 42
|Ignore all arguments                             |
(g '(1 2 3 4))                ===> 6
|Some values: 123, etc.                           |
(g '(1 2 3))                  ===> 6
|Some values: 123, etc.                           |
(g '(1 2))                    ===> <execution terminated>
|(<-lambda): actual arguments do not match formals|
(g 1 2)                       ===> <execution terminated>
|(<-lambda): actual arguments do not match formals|
(g)                           ===> <execution terminated>
|(<-lambda): actual arguments do not match formals|
```
--------------
library syntax: `(curried-lambda <formals> <body>)`

library syntax: `(arity <formals>)`

`<formals>` and `<body>` are the same as per `(lambda)`.

`(arity <formals>)` evaluates to a number:

* for fixed argument set `(a_1 ... a_n)`, it evaluates to the number of arguments _n_;
* for more-than-n argument set `(a_1 ... a_n . a_rest)`, it evaluates to the amount of arguments to the left of the dot, _n_;
* for variadic list `a_any`, it evaluates to 0.

`(curried-lambda <formals> <body>)` evaluates to a procedure that acts in the following way:

* when `<formals>` is a fixed set `(a_1 ... a_n)`, then the procedure
    * if called with less than _n_ arguments `(b_1 ... b_m)`, _m_&lt;_n_,<br/>
      returns a procedure that acts like `(curried-lambda (a_m+1 ... a_n) (proc b_1 ... b_m a_m+1 ... a_n))`,
    * if called with exactly _n_ arguments `(b_1 ... b_n)`, returns `(proc b_1 ... b_n)`,
    it is an error if the procedure is called with more than _n_ arguments;
* when `<formals>` is a more-than-n set `(a_1 ... a_n . a_rest)`, then the procedure
    * if called with less than _n_ arguments `(b_1 ... b_m)`, _m_&lt;_n_,<br/>
      returns a procedure that acts like<br/>
      `(curried-lambda (a_m+1 ... a_n . a_rest) (apply proc b_1 ... b_m a_m+1 ... a_n a_rest))`,
    * if called with _n_ or more arguments `(b_1 ... b_k)`, k≥_n_, returns `(proc b_1 ... b_k)`;
* when `<formals>` is a variadic list, `(curried-lambda <formals> <body>)` is equivalent to `(lambda <formals> <body>)`.

#### Example:
```scheme
(define f (curried-lambda (a b c) (+ a (* b c))))
(f)                          ===> #<PROCEDURE>
(f 1)                        ===> #<PROCEDURE>
(f 1 2)                      ===> #<PROCEDURE>
(f 1 2 3)                    ===> 7
(((f 1) 2) 3)                ===> 7
(define g (f 2 3))
(g 4)                        ===> 14
;(report (f 1 2 3 4))        ; error: Wrong number of arguments passed to procedure
(define f (curried-lambda (a b . c) (println "A: " a ", B: " b ", etc: " c)))
(f 1)                        ===> #<PROCEDURE>
(f 1 2)                      ===>
|A: 1, B: 2, etc: () |
(f 1 2 3)                    ===>
|A: 1, B: 2, etc: (3)|

(arity (x y))                       ===> 2
(arity (x y . args))                ===> 2
(arity args)                        ===> 0
```
### 4.1.5. Conditionals

library syntax: `(if-identifier <expr> <then> <else>)`

This form, borrowed from [http://okmij.org/ftp/Scheme/macros.html#macro-symbol-p](http://okmij.org/ftp/Scheme/macros.html#macro-symbol-p), expands to `<then>` if `<expr>` is a single identifier, and to `<else>` otherwise.

#### Example:
```scheme
(if-identifier x 'id 'expr)                      ===> id
(if-identifier (+ x 1) 'id 'expr)                ===> expr
```
### 4.1.6. Assignments

library syntax: `(<-! <pattern> <expression>)`

This is the pattern-matching version of `(set!)`.

All pattern variables should be `(define)`d earlier. `<expression>` is evaluated; if its value matches `<pattern>`, pattern variables are assigned corresponding (sub...)expressions; if `<expression>`'s value does not match `<pattern>`, pattern variables are left untouched. Unlike other !-forms and procedures, `(<-!)` returns a boolean value: `#t` if `<expression>` matches `<pattern>`, and `#f` otherwise. See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(define a #f) (define b #f) (define c #f) (define d #f) (define e #f)
(println a " || " b " || " c " || " d " || " e)                                ===>
|#f || #f || #f || #f || #f                                     |
(<-! (a (b @ (c #(d e)))) 'does-not-match)                                     ===> #f
(println a " || " b " || " c " || " d " || " e)                                ===>
|#f || #f || #f || #f || #f                                     |
(<-! (a (b @ (c #(d e)))) '(42 (matches #("Hello," "world!"))))                ===> #t
(println a " || " b " || " c " || " d " || " e)                                ===>
|42 || (matches #(Hello, world!)) || matches || Hello, || world!|
```
### 4.2.1. Conditionals

library syntax: `(<-case <key> <clause1> <clause2>...)`

`<key>` may be any expression. Each `<clause>` should have the form `((<pattern>...) <expression1> <expression2>...)`. The last `<clause>` may be an "else clause" which has the form `(else <expression1> <expression2>...)`. `<key>` is evaluated and its result is matched against each `<pattern>`, in order. If there is a match, then the `<expression>`s in the `<clause>` are evaluated from left to right and the results of the last `<expression>` are returned as the results of the `(<-case)` expression. `<pattern>`s of the same `<clause>` can have different sets of pattern variables, but nevertheless all the variables referred to by the `<expression>`s of matching `<clause>` should be bound, either by pattern-matching, or in the current environment. If none of the `<pattern>`s matches but there is an "else clause", its `<expression>`s are evaluated and the results of the last `<expression>` are the results of the `(<-case)` expression; otherwise the result of the `(<-case)` expression is unspecified.

See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(define a "non applicable")
(define (disp-x x)
  (<-case x
    (((a . _) ()) (println "X is a pair/list, and its car field is: " a) 'pair)
    ((#(: _ :)) (println "X is a vector") 'vector)
    (else (println "X is some unknown object") 'unknown)))
(disp-x '(42))                    ===> pair
|X is a pair/list, and its car field is: 42            |
(disp-x '())                      ===> pair
|X is a pair/list, and its car field is: non applicable|
(disp-x '#(1 2 3))                ===> vector
|X is a vector                                         |
(disp-x 42)                       ===> unknown
|X is some unknown object                              |
```
### 4.2.2. Binding constructs

library syntax: `(<-if-let ((<pattern> <expr>)...) <consequent> <alternate>)`

library syntax: `(<-if-let* ((<pattern> <expr>)...) <consequent> <alternate>)`

When all `<expr>`s match corresponding `<pattern>`s, pattern variables are bound (in the sense of `(let)` and `(let*)`, respectively) to the corresponding (sub...)expressions, `<consequent>` is evaluated in the extended environment and its values are returned. When an `<expr>` does not match its `<pattern>`, `<alternate>` is evaluated in the current environment and its values are returned. Note that though `(<-if-let*)` allows for pattern variables to refer to the pattern variables introduced in earlier bindings, this is not possible for them to refer to pattern variables from the same `<pattern>`, since it would be a circular reference in the corresponding `<expr>`.

**Note:** currently, there is no such thing as `(<-if-letrec)`. It is unclear whether it can be defined concisely without enormous effort and compiler magic.

See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(<-if-let (((s . n) (cons "Hello, world!" 42))) (begin (println s) n) 84)                ===> 42
|Hello, world!|
(<-if-let (((s n) (cons "Hello, world!" 42))) (begin (println s) n) 84)                  ===> 84
```
--------------
library syntax: `(<-let ((<pattern> <expr>)...) <body>)`

library syntax: `(<-let* ((<pattern> <expr>)...) <body>)`

When all `<expr>`s match corresponding `<pattern>`s, pattern variables are bound (in the sense of `(let)` and `(let*)`, respectively) to the corresponding (sub...)expressions, `<body>` is evaluated in the extended environment and its values are returned. When an `<expr>` does not match its `<pattern>`, the value of the form is unspecified. Note that though `(<-let*)` allows for pattern variables to refer to the pattern variables introduced in earlier bindings, this is not possible for them to refer to pattern variables from the same `<pattern>`, since it would be a circular reference in the corresponding `<expr>`.

**Note:** currently, there is no such thing as `(<-letrec)`. It is unclear whether it can be defined concisely without enormous effort and compiler magic.

See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(<-let*
  (((a . b) '(1 2 3))
   ((c _) b)
   ((string #\H #\e d e : owor : _ _ _) "Hello, world!"))
  (list a b c d e owor))                                     ===> (1 (2 3) 2 #\l #\l "o, wor")
(<-let (((a b c) '(will not match: five elements))) 42)      ===> unspecified
```
--------------
library syntax: `(rename ((identifier value) ...) expr)`

This form is expanded into the expression which is expr with each ocurrence of identifier being replaced with the corresponding value. I borrowed this macro from some place I don't remember, sorry.

#### Example:
```scheme
(rename ((x a) (y b)) '(x y))           ===> '(a b)
```
--------------
library syntax: `(same-name ((identifier value) ...) expr)`

Complementary to `(rename)`, this form simply expands into `expr`.

#### Example:
```scheme
(same-name ((x a) (y b)) '(x y))                ===> '(x y)
```
--------------
library syntax: `(where expr (variable init) ...)`

Complementary to `(let)`, this form evaluates `expr` with `init`s supplied for `variable` values. It is in all senses equivalent to `(let ((variable init)...) expr)`.

#### Example:
```scheme
(where (+ a b) (a 40) (b 2))       ===> 42
```
## 6.1. Equivalence predicates

library syntax: `(<-equal? <pattern> <expr>)`

Evaluates to `#t` if `<expr>` matches `<pattern>` and #f otherwise. All pattern variables should be `(define)`d in current environment and their values are compared against corresponding (sub...)expressions. This is the only pattern-matching form that does not bind or assign values to pattern variables.

See [README.patterns](README.patterns.md) for more.

#### Example:
```scheme
(define x 42)
(<-equal? x 84)                                                        ===> #f
(<-equal? #(x : _) (vector (+ 40 2) "Hello," "world!"))                ===> #t
```
### 6.3.2. Pairs and lists

library procedure: `(all? proc? list1 list2...)`

library procedure: `(any? proc? list1 list2...)`

`proc?` should accept as many arguments as there are `list`s given. These procedures both return boolean values. `(all?)` returns `#t` iff `(proc?)` returns logical true element-wise for all elements of the lists, and `(any?)` return `#f` iff `(proc?)` returns logical false element-wise for all elements of the lists. Note that `(any?)` differs in semantics from `(memp)` defined in R7RS.

#### Example:
```scheme
(all? < '(1 2 3) '(2 3 4))		===> #t
(any? > '(1 2 3) '(2 3 4))		===> #f
```
--------------
library procedure: `(concat list)`

The elements of `list` should be lists. Returns the concatenation of all elements of `list`, in order, i.e. the same, in the sense of `(equal?)`, as `(apply append list)`.

#### Example:
```scheme
(concat (list '(1 2) (list 3 4 5) (string->list "Hello!")))                ===> (1 2 3 4 5 #\H #\e #\l #\l #\o #\!)
```
--------------
library procedure: `(drop-until proc? list1 list2...)`

library procedure: `(drop-while proc? list1 list2...)`

`proc?` should accept as many arguments as there are lists. These procedures return the suffixes of the lists with all the first elements, such that `(proc?)` returns `#f`, for drop-until, and `#t`, for drop-while, elementwise, removed. Note that `(drop-until proc? list)` is the same as `(memp proc? list)` introduced in R7RS. `(append (take-while proc? list) (drop-until? ($* not proc?) list))`, as well as `(append (take-until proc? list) (drop-while? ($* not proc?) list))`, is the same, in the sense of `(equal?)`, as `list` itself; for multiple lists this is true up to `(values)` and `(call-with-values)` or `($*)`.

#### Example:
```scheme
(drop-until even? '(1 3 5 2 4 6 7 9 11))                                                                ===> (2 4 6 7 9 11)
(call-with-values (lambda () (drop-while < '(1 2 3 4 5 6 7 8) '(5 6 7 8 -1 -2 -3 -4))) cons)            ===> ((5 6 7 8) -1 -2 -3 -4)
```
--------------
library procedure: `(drop k list1 list2...)`

Returns _k_th tail of each list. Note that `(drop k list)` is the same, in the sense of `(equal?)`, as `(list-tail list k)`.

#### Example:
```scheme
(drop 2 '(1 2 3 4))                                                                             ===> (3 4)
(call-with-values (lambda () (drop 2 '(0 1 2 3) '(20 21 22 23) '(40 41 42 43))) show)           ===> "(2 3)(22 23)(42 43)"
```
--------------
library procedure: `(fold-left proc obj list1 list2...)`

library procedure: `(fold-right proc obj list1 list2...)`

`proc` should accept as many arguments as there are `list`s given, plus one.

When `list`s are empty, obj is returned.

When `list`s are non-empty, `(fold-left proc obj list1 list2...)` returns `(fold-left proc (proc obj (car list1) (car list2)...) (cdr list1) (cdr list2)...)`, recursively, and `(fold-right proc obj list1 list2...)` returns `(proc (car list1) (car list2)... (fold-right proc obj (cdr list1) (cdr list2)...))`. Both results are `(equal?)` for commutative functions.

#### Example:
```scheme
(fold-left + 0 '(1 2 3 4))                                                            ===> 10
(fold-left string-append "Hel" '("lo," " wo" "rld" "!"))                              ===> "Hello, world!"
(fold-left (lambda (c t e) (if c t e)) #f '(1 2 #f 4) '(#f #t #t #f))                 ===> #f
(fold-right + 0 '(1 2 3 4))                                                           ===> 10
(fold-right string-append "rld!" '("Hel" "lo," " wo"))                                ===> "Hello, world!"
(fold-right (lambda (t e c) (if c t e)) #f '(1 2 #f 4) '(#f #t #t #f))                ===> 1
```
--------------
library procedure: `(foldl proc list)`

library procedure: `(foldr proc list)`

`proc` should accept two arguments. `(foldl proc list)` is the same as `(fold-left proc (car list) (cdr list))` and `(foldr proc list)` is the same as `(fold-right proc (last list) (init list))`. It is an error if `list` is empty. Results are `(equal?)` for associative operation.

#### Example:
```scheme
(foldl string-append '("Hel" "lo," " wo" "rld!"))                ===> "Hello, world!"
(define (non-associative x y) (+ (* 2 x) y))
(foldl non-associative '(1 2 3 4))                               ===> 26
(foldr non-associative '(1 2 3 4))                               ===> 16
```
--------------
library procedure: `(init list)`

Returns a fresh list containing all but the last elements of `list`. It is an error is `list` is empty.

#### Example:
```scheme
(init '(1 2 3 4))                ===> (1 2 3)
```
--------------
library procedure: `(last list)`

Returns the last element of the `list`. It is an error if `list` is empty.

#### Example:
```scheme
last '(1 2 3 4))                ===> 4
```
--------------
library procedure: `(init-last list)`

Returns a fresh list contaning all but the last elements of the `list` and the last element of the `list`. It is an error if list is empty.

#### Example:
```scheme
(call-with-values (lambda () (init-last '(1 2 3 4))) cons)                ===> ((4 3 2) . 1)
```
--------------
library procedure: `(list-head list k)`

Returns a fresh list that consists of `k` first elements of `list`, in order; complementary to `(list-tail)` defined in R5RS. It is an error if `list` has less than `k` elements.

#### Example:
```scheme
(list-head '(42 43 44) 1)                ===> (42)
```
--------------
library procedure: `(take k list1 list2...)`

Returns prefixes of length `k` of all lists. `(take k list)` is the same, in the sense of `(equal?)`, as `(list-head list k)`.

#### Example:
```scheme
(take 3 '(40 41 42 43 44 45))                                                          ===> (40 41 42)
(call-with-values (lambda () (take 2 '(a b) '(c d e) '(f g h i))) show)                ===> "(a b)(c d)(f g)"
```
--------------
library procedure: `(seq-list begin end)`

library procedure: `(seq-list begin end step)`

`begin`, `end` and `step` should be real numbers. Builds a list of numbers starting from `begin` and going to `end`. `step` can be negative, but not zero. `end` is not included in the list.

This function is rather stack-hungry, unless your implementation of choice knows tail recursion modulo cons. This is not required by R5RS; in fact, these words are not even found in the document. In my tests, gsi built a list of 3000 integers rather fast, while guile crashed.

#### Example:
```scheme
(seq-list 0 10)                 ===> (0 1 2 3 4 5 6 7 8 9)
(seq-list 2 -3.14 -.5)          ===> (2 1.5 1. .5 0. -.5 -1. -1.5 -2. -2.5 -3.)
(seq-list 0 0 0)                ===>
|(seq-list): zero step would yield infinite list|
|<program execution aborted>                    |
```
--------------
library procedure: `(take-until proc? list1 list2...)`

library procedure: `(take-while proc? list1 list2...)`

`proc?` should accept as many arguments as there are `list`s. Given lists `l_1 ... l_k` with elements `l_1_1 ... l_k_n` these procedures returns the longest prefixes of equal length _len_ such that if _i_≤_len_ then `(proc? l_1_i ... l_k_i)` is logical false for `take-until`, logical true in case of `take-while`. Note that `(take-while proc? list1 list2...)` is the same as `(take-until ($* not proc?) list1 list2...)`.

#### Example:
```scheme
(take-until even? '(1 3 5 7 8 9 10 11 12))                                                         ===> (1 3 5 7)
(call-with-values (lambda () (take-until < '(10 9 8 7) '(8 8 8 8) '(3 5 7 9))) show)               ===> "(10 9 8)(8 8 8)(3 5 7)"
(take-while odd? '(1 3 5 7 8 9 10 11 12))                                                         ===> (1 3 5 7)
(call-with-values (lambda () (take-while < '(1 2 4 7) '(4 4 4 4) '(8 6 4 2))) show)               ===> "(1 2)(4 4)(8 6)"
```
### 6.3.5. Strings

library procedure: `(obj->string obj)`

This is generic `->string`-method. It converts its argument to string, by reasonable rules (e.g. using `(number->string)` for numbers).

#### Example:
```scheme
(obj->string #t)                                                                        ===> "#t"
(obj->string (cons 1 (cons 2 (cons 3 4))))                                              ===> "(1 2 3 . 4)"
(obj->string (vector 3.141593 (+ 10 32) (string-append Hello,  world!)))                ===> "#(3.141593 42 Hello, world!)"
```
--------------
library procedure: `(show obj...)`

Converts its every argument to string, in the sence of `(obj->string)`, and returns the concatenation of those strings.

#### Example:
```scheme
(show #\H "ello, " '(w (o) r) '#(l d "!") 42)         ===> "Hello, (w (o) r)#(l d !)42"
(show 'a 'b 'c 'd)                                     ===> "abcd"
```
--------------
library procedure: `(source obj)`

Converts its argument to string containing argument's external representation. For most R5RS type it behaves like `(obj->string)`, but turns characters into char literals and wraps strings in doublequotes, escaping doublequote and backslash characters.

#### Example:
```scheme
(source '(1 #(3.14) "Hello, \"world\"\\!"))              ===> "(1 #(3.14) \"Hello, \\\"world\\\"\\\\!\")"
```
### 6.3.6. Vectors

library procedure: `(subvector vec k1)`

library procedure: `(subvector vec k1 k2)`

`k1` and `k2` should be valid indices of vec, `k1`≤`k2`. When `k2` is omitted it defaults to `(vector-length vec)`. The procedure returns a fresh vector consisting of all the elements of `vec`, starting with index `k1` and ending with (`k2`-1). If `k1` and `k2` are equal, the resulting vector is empty.

#### Example:
```scheme
(subvector '#(a b c d e f g) 3)                        ===> #(d e f g)
(subvector '#(a b c d e f g) 3 6)                      ===> #(d e f)
(subvector '#(a b c d e f g) 3 (+ 1 2))                ===> #()
```
## 6.4. Control features

library procedure: `($% proc)`

It is analogous to Haskell's flip. proc should accept at least two arguments. `($% proc)` is a procedure that, when called with arguments `x_1 x_2 ... x_n`, _n_ > 1, evaluates `(proc x_2 x_1 ... x_n)` and returns its values.

The name consists of here-conventional combinator flag `$` and `%`, that symbolizes arguments flip graphically.

#### Example:
```scheme
(($% string-append) "A" "B" "C" "D")            ===> "BACD"
```
--------------
library procedure: `($* proc...)`

This is functional composition operator.

With no arguments, returns values (functional "one").

With one argument, return its argument (functional linear polynome).

When more than one argument is given, every proc should accept as much arguments as the next proc to its right returns values. `($* proc<1> ... proc<n-1> proc<n>)` works this way: first `proc<n>` is called with whatever arguments provided to the procedure; then a tail call is performed to `($* proc<1> ... proc<n-1>)` with values returned by `proc<n>` as arguments.

`(values)` is functional "one" (identity): for any `f`, `($* f values)` is the same as `($* values f)` which is the same as `f`. In fact, any occurence of `values` can be safely removed from the list of arguments to `($*)`, since it does not have side effects.

The name consists of here-conventional combinator flag `$` and `*`, multiplication asterisk, that sometimes serves as composition in functional notation.

#### Example:
```scheme
(($* ($+ + 2) ($+ * 4)) 10)                               ===> 42
(($* + values) 1 2 3 4)                                   ===> 10
(($* values values + values values) 1 2 3 4)              ===> 10
(define zip-if ($* ($+ map cons) ($+ filter)))
(zip-if < '(1 10 2 20 3 30) '(40 4 50 5 60 6))            ===> ((1 . 40) (2 . 50) (3 . 60))
```
--------------
library procedure: `($+ proc obj...)`

This is partial application combinator. `($+ proc x_1 ... x_n)` is the procedure that, when called with arguments `y_1 ... y_m`, performs a tail call to `(proc x_1 ... x_n y_1 ... y_m)`. It is an error if proc does not accept n+m arguments.

The name consists of here-conventional combinator flag `$`, and `+`, plus means that the new function is the old one plus some fixed arguments.

#### Example:
```scheme
(($+ + 1 2) 3 4 32)                            ===> 42
(define f ($+ string-append "Hell" "o, "))
(f "world" "!")                                ===> "Hello, world!"
(f)                                            ===> "Hello, "
(define mk-triple ($+ make-vector 3 1))  ; kind of Factory
(mk-triple)                                    ===> #(1 1 1)
(define mk-quad ($+ make-vector 4))
(mk-quad 1)                                    ===> #(1 1 1 1)
(mk-quad 2)                                    ===> #(2 2 2 2)
```
--------------
library procedure: `($0 obj1 obj2...)`

Returns a procedure that accepts an arbitrary number of arguments and returns `obj`s provided. It is similar to Haskell's `const`, but can accept more than one value (and then the procedure created would return several values, respectively).

Except for multiple values returned, this is basically K-combinator.

The name consists of here-conventional combinator flag `$`, and `0`, as constant function is kind of functional `zero`.

#### Example:
```scheme
(($0 "Hello, world!") 1 2 3 42)                 ===> "Hello, world!"
(define f ($0 'a 'b 'c))
(call-with-values f (lambda args args))         ===> (a b c)
```
--------------
library procedure: `($@ proc obj1...)`

Calls `proc` with arguments `obj1...` Unlike in `(apply)`, the last argument to `$@` is the last argument to `proc`, not the rest-arguments list.

The name consists of here-conventional combinator flag `$`, and `@`, here read as `at`: proc is computed `at` the argument values given.

#### Example:
```scheme
($@ +)                                                                   ===> 0
($@ + 1 2 3)                                                             ===> 6
(map $@ (list + * make-vector) '(1 2 3)) '(-1 3.14159 42))               ===> (0 6.28318 #(42 42 42))
```
--------------
library procedure: `(curry proc k)`

`proc` should accept `k` or more arguments. Returns `k`-curried version of `proc`. `k`-curried version is a procedure that works in the following way:

* when called with arguments `a1 ... al`, _l_&lt;`k`, returns (`k`-_l_)-curried version of `($+ proc a1 ... al)`;
* when called with `k` or more arguments `b1 ... bm`, *m*≥`k`, returns `(proc b1 ... bm)`.

(Of course, it is an error, in the second case, if `proc` does not accept _m_ arguments.)

**Note**: `(curried-lambda)` is provided to define a fresh fully curried functions with automatically deduced arity.

#### Example:
```scheme
(define plus-some (curry + 3))
(define plus-1-2 (plus-some 1 2))
(plus-1-2 3)                        ===> 6
(plus-some 1 2 3 4)                 ===> 10
(plus-some 1)                       ===> #<PROCEDURE>
```
Note that if `proc` normally would accept less than `k` arguments, this call is unavailable for the `k`-curried version:
```scheme
(define to-port (curry display 2))
(define print-hello (to-port "Hello!"))
print-hello                                        ===> #<PROCEDURE>
(print-hello (current-output-port))                ===>
|Hello!|
```
--------------
library syntax: `(do-for <gens> <expr1> <expr2>...)`

library syntax: `(for <gens> <expr1> <expr2>...)`

List comprehension. `(for)` returns a fresh list and `(do-for)` returns an unspecified value. `<gens>` should have form `(<gen1> <gen2>...)` where each `<gen>` is either:

* `(<variable> list)`
* `(<pattern> <- list)`
* `(let <bindings>)`
* `(let* <bindings>)`
* `(letrec <bindings>)`
* `(<-let <pattern-bindings>)`
* `(<-let* <pattern-bindings>)`
* `(<-if-let <pattern-bindings>)`
* `(<-if-let* <pattern-bindings>)`
* `(<gen> ? <expr1> <expr2>...)`
* `(<gen> ?? <expr1> <expr2>...)`
* `(<gen> ? <expr> ?? <expr1> <expr2>...)`
* `(<gen> ?? <expr> ? <expr1> <expr2>...)`

For each set of variables formed by traversing lists in generators and binding variables to values in let-declarations, `(do-for)` executes `<expr>`s for their side-effects, and `(for)` executes `<expr>`s and puts the value of the last one into resulting list. See [README.comprehension](README.comprehension.md) for more.

#### Example:
```scheme
(for ((x '(1 2 3)) (<-let ((_ . upper) (seq-list x 5))) (y upper)) (show x y))                ===> ("12" "13" "14" "23" "24" "34")
(do-for (((string x : xs : y) <- '("Hello" "world"))) (println y xs x))                       ===>
|oellH|
|dorlw|
```
--------------
library procedure: `(error obj...)`

This procedure has been introduced as a workaround for the fact that R5RS does not define any mechanism for error signalling. It `(println)`s its arguments and then throws an error interrupting program execution.

#### Example:
```scheme
(error "Program interrupted, reason: " 42)        ===>
|Program interrupted, reason: 42|
|<program execution aborted>    |
```
--------------
library procedure: `(filter proc list1 list2...)`

`proc` should accept as many arguments as there are `list`s given. With one list given, returns a fresh list that contains all elements of the argument that satisfy `proc` (i.e. for which `proc` does not return `#f`). With multiple `list`s `(a_1_1 ... a_1_n) ... (a_k_1 ... a_k_n)` returns fresh lists `(a_1_i<1> ... a_1_i<m>) ... (a_k_i<1> ... a_k_i<m>)` such that for every _j_ `(proc a_1_i<j> ... a_k_i<j>)` is not `#f`.

Current implementation is in accordance with the rules for the `(map)`, i.e. for R5RS implementation all the lists should be of the same length, but R6RS does not require that.

#### Example:
```scheme
(filter odd? '(1 2 3 4 5 6 7))                                                                  ===> (1 3 5 7)
(call-with-values (lambda () (filter < '(1 20 3 40 5 60 7) '(10 2 30 4 50 6 70))) show)         ===> "(1 3 5 7)(10 30 50 70)"
```
--------------
library procedure: `(flat-map proc list1 list2...)`

`proc` should accept as many arguments as there are `list`s given and return a list for every argument set contained in `list`s. Returns the concatenation of results of `proc` applied element-wise to `list`s. `list`s should be of equal length in R5RS.

#### Example:
```scheme
(list->string (flat-map string->list '("Hello" ", " "world" "!")))                ===> "Hello, world!"
```
--------------
library procedure: `(for-each-if proc if? list1 list2...)`

library procedure: `(for-each-while proc while? list1 list2...)`

`proc` and `if?`/`while?` are procedures accepting as many arguments as there are `list`s given. These procedures apply `proc` to corresponding elements of some subsequence of the `list`s. Given `list`s `l_1 ... l_k` with elements `(l_1_1 ... l_1_n) ... (l_k_1 .., l_k_n)`, subsequence for `(for-each-if)` consists of all layers `(l_1_i ... l_k_i)`, 0&gt;_i_≤_n_, satisfying `if?`, i.e. such that `(if? l_1_i ... l_k_i)` is not false. The subsequence for `(for-each-while)` is the longest prefix of the `list`s such that all elements of this prefix satisfy `while?`.

In other words, `(for-each-if proc if? list1 list2...)` is the same as `(($* ($+ for-each proc) ($+ filter if?)) list1 list2...)` and `(for-each-while proc while? list1 list2...)` is the same as `(($* ($+ for-each proc) ($+ take-while while?)) list1 list2...)`.

These procedures are used in for-comprehension.

#### Example:
```scheme
(for-each-if print odd? '(1 2 3 4 5 6 7)) (println)                                           ===>
|1357|
(for-each-if ($* print cons) < '(1 20 3 40 5 60 7) '(10 2 30 4 50 6 70)) (println)            ===>
|(1 . 10)(3 . 30)(5 . 50)(7 . 70)|
(for-each-while print odd? '(1 3 5 2 4 6 7 9)) (println)                                      ===>
|135|
(for-each-while ($* print cons) < '(1 2 3 40 5 60 7) '(10 20 30 4 50 6 70)) (println)         ===>
|(1 . 10)(2 . 20)(3 . 30)|
```
--------------
library procedure: `(for-each-if-while proc if? while? list1 list2...)`

library procedure: `(for-each-while-if proc while? if? list1 list2...)`

`proc`, `while?` and `if?` are procedures accepting as many arguments as there are `list`s given. `if?` is a symbolic name for the predicate that serves as the first argument to `(filter)`, and `while?` is that for `(take-while)`; `(filter)` and `(take-while)` are applied rightmost-first. The second action is performed on the output of the first one, and the `proc` is applied to each element layer of the second output.

In other words, `(for-each-if-while proc if? while? list1 list2...)` is the same as `(($* ($+ for-each proc) ($+ filter if?) ($+ take-while while?)) list1 list2...)` and `(for-each-while-if proc while? if? list1 list2...)` is the same as `(($* ($+ for-each proc) ($+ take-while while?) ($+ filter if?)) list1 list2...)`.
These procedures are used in for-comprehension.

#### Example:
```scheme
(for-each-if-while ($+ + 1) odd? ($+ > 6) '(1 3 5 2 4 6 7 9))		===> (2 4 6)
(for-each-if-while cons < ($* not =) '(1 2 3 40 5 6 7) '(10 20 30 4 50 6 70))		===> ((1 . 10) (2 . 20) (3 . 30) (5 . 50))
(for-each-while-if ($+ + 1) odd? ($+ > 6) '(1 3 2 5 4 6 7 9))		===> (2 4)
(for-each-while-if cons < ($* not =) '(1 2 6 3 40 5 7) '(10 20 6 30 4 50 70))		===> ((1 . 10) (2 . 20) (3 . 30))
```
--------------
library procedure: `(id obj)`

Returns `obj`. This is an unary versiong of `(values)`, defined for performance. In most places inside the prelude.scm where identity function would take place, `(values)` is used.

#### Example:
```scheme
(id 42)         ===> 42
```
--------------
library procedure: `(map-if proc if? list1 list2...)`

library procedure: `(map-while proc while? list1 list2...)`

`proc` and `if?`/`while?` are procedures accepting as many arguments as there are `list`s given. These procedures return a fresh list consisting of results of applying `proc` to corresponding elements of some subsequence of the `list`s. Given `list`s `l_1 ... l_k` with elements `(l_1_1 ... l_1_n) ... (l_k_1 .., l_k_n)`, subsequence for `(map-if)` consists of all layers `(l_1_i ... l_k_i)`, 0&gt;_i_≤_n_, satisfying `if?`, i.e. such that `(if? l_1_i ... l_k_i)` is not false (and then `(map-if)` returns list whose elements are `(proc l_1_i ... l_k_i)` for all such _i_). The subsequence for `(map-while)` is the longest prefix of the `list`s such that all elements of this prefix satisfy `while?`.

In other words, `(map-if proc if? list1 list2...)` is the same as `(($* ($+ map proc) ($+ filter if?)) list1 list2...)` and `(map-while proc while? list1 list2...)` is the same as `(($* ($+ map proc) ($+ take-while while?)) list1 list2...)`.

The order in which proc is applied to the element layers is undefined, as is true for `(map)`.

These procedures are used in list comprehension.

#### Example:
```scheme
(map-if show odd? '(1 2 3 4 5 6 7))                                     ===> ("2" "4" "6" "8")
(map-if cons < '(1 20 3 40 5 60 7) '(10 2 30 4 50 6 70))                ===> ((1 . 10) (3 . 30) (5 . 50) (7 . 70))
(map-while ($+ + 1) odd? '(1 3 5 2 4 6 7 9))                            ===> (2 4 6)
(map-while cons < '(1 2 3 40 5 60 7) '(10 20 30 4 50 6 70))             ===> ((1 . 10) (2 . 20) (3 . 30))
```
--------------
library procedure: `(map-if-while proc if? while? list1 list2...)`

library procedure: `(map-while-if proc while? if? list1 list2...)`

`proc`, `while?` and `if?` are procedures accepting as many arguments as there are `list`s given. `if?` is a symbolic name for the predicate that serves as the first argument to `(filter)`, and `while?` is that for `(take-while)`; `(filter)` and `(take-while)` are applied rightmost-first. The second action is performed on the output of the first one, and the `proc` is applied to each element layer of the second output. The procedure returns the list of results of `proc`.

In other words, `(map-if-while proc if? while? list1 list2...)` is the same as `(($* ($+ map proc) ($+ filter if?) ($+ take-while while?)) list1 list2...)` and `(map-while-if proc while? if? list1 list2...)` is the same as `(($* ($+ map proc) ($+ take-while while?) ($+ filter if?)) list1 list2...)`.

The order in which proc is applied to the element layers is undefined, as is true for `(map)`.

These procedures are used in list comprehension.

#### Example:
```scheme
(map-if-while (show) odd? ($+ > 6) '(1 3 5 2 4 6 7 9))                                  ===> ("1" "3" "5")
(map-if-while cons < ($* not =) '(1 2 3 40 5 6 7) '(10 20 30 4 50 6 70))		===> ((1 . 10) (2 . 20) (3 . 30) (5 . 50))
(map-while-if (show) odd? ($+ > 6) '(1 3 2 5 4 6 7 9))                                  ===> ("1" "3")
(map-while-if cons < ($* not =) '(1 2 6 3 40 5 7) '(10 20 6 30 4 50 70))		===> ((1 . 10) (2 . 20) (3 . 30))
```
--------------
library syntax: `(variable? expr)`

library syntax: `(variable? expr)`

Probably not much of a value, this form evaluates to true if `expr` is an identifier, and false otherwise. `expr` is never evaluated and can even refer to un`(define)`d variables.

#### Example:
```scheme
(variable? x)                   ===> #t
(variable? (+ 3 2))             ===> #f
```
--------------
6.6. Input and output

library procedure: `(fprint port obj ...)`

library procedure: `(fprintln port obj ...)`

`fprint` `(display)`s all its arguments starting from the second, in order, to `port`. `fprintln` does the same and then `(newline)`s.
--------------
library procedure: `(print obj ...)`

library procedure: `(println obj ...)`

Same as `fprint`/`fprintln` but uses (current-output-port).

#### Example:
```scheme
(print "Hello" ", ") (println "world" #\!) (println)        ===>
|Hello, world!|
|             |
```
library syntax: `(report obj)`

library syntax: `(report arrow expr ...)`

When multiple arguments specified, report `(print)`s every one but the first expression's `(source)`, then the first one expression (`arrow`), which is considered to be some kind of delimiter to separate expressions from values, and then `(println)`s the values of expressions. With one argument, `(report obj)` is the same as `(report "<tab><tab>===> " obj)`, i.e. it uses the same arrow as separator that can be seen in examples in the text of R5RS. Here `<tab>` denotes tab character, not a sequence of characters #\<, #\t, #\a, #\b and #\>.

#### Example:
```scheme
(report (+ 1 2))                    ===>
|(+ 1 2)       ===> 3|
(report ": " (+ 1 2) (+ 3 4))       ===>
|(+ 1 2)(+ 3 4): 37|
```
