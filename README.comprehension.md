Some very common list comprehension elements have been implemented in prelude.scm. Similar to those in Clojure, they come in the form of `(for)` and `(do-for)` syntaxes.

## I. Syntaxes

A comprehension syntax has the following form:
```scheme
(<keyword> <generators> <body>)
```
`<keyword>` is one of `for` or `do-for` (`for-each` is a standard library procedure in R5RS). `(for)` collects all the generated values in a single flat list and returns that list. `(do-for)` executes expressions for every generated set for their side effects and returns unspecified value.

`<generators>` are of the form:
```scheme
(<generator1> <generator2>...)
```
The different kinds of generators are covered below. At this moment, let's say a few words about the order of generation.

Generators are triggered left-to-right, and list generators are rolling like the digits on odometer: rightmost fastest. The result of the expression
```scheme
(for ((x '(1 2 3)) (y '(4 5 6))) (cons x y))
```
is the list of pairs
```scheme
(1 . 4)
(1 . 5)
(1 . 6)
(2 . 4)
(2 . 5)
```
and so on. The resulting list is built in such order that for every value of x, all values of y are enumerated first.

Every generator produces some set of variable bindings. When generator produces a list, values are assigned to the variables of this set for every element of the list; this process is called list traversal. The union of all such sets for a for-expression is the bindings set to the for-expression. Every set of values, that those bindings are filled with, is called a generated set. Variable bindings introduced in a generator can be overriden by the following generators, to the right; they even can be overriden in the same generator, if, for example, it is a (let*) generator. In evaluation of <body>, the rightmost binding for every variable is used (when there is no such a binding amongst generators, the binding from the current environment is used).

`<body>` is non-empty sequence of expressions, as usual.

### 1. `(for)`
library syntax: `(for <generators> <body>)`

Returns a fresh list. For every generated set, `<body>` is evaluated, and the value of its last expression is added to the list. Returns an empty list if there were no generated sets.

#### Example:
```scheme
(for ((x (seq-list 0 3)) (y (seq-list 0 3))) (show x y))                ===> ("00" "01" "02" "10" "11" "12" "20" "21" "22")
(for ((x '(2 4 6) ? (odd? x))) x)                                       ===> ()
```
### 2. `(do-for)`
library syntax: `(do-for <generators> <body>)`

For every generated set, if any, `<body>` is evaluated for its side-effects. The return value is unspecified.
#### Example:
```scheme
(do-for ((x (seq-list 0 3)) (y (seq-list (* x 3) (+ (* x 3) 3)))) (println "trunc(" y " / 3) = " x))                ===>
|trunc(0 / 3) = 0|
|trunc(1 / 3) = 0|
|trunc(2 / 3) = 0|
|trunc(3 / 3) = 1|
|trunc(4 / 3) = 1|
|trunc(5 / 3) = 1|
|trunc(6 / 3) = 2|
|trunc(7 / 3) = 2|
|trunc(8 / 3) = 2|
```
## II. Generators

### 1. List generator
auxiliary syntax: `(<identifier> <expr>)`

`<expr>` should evaluate to a list. Bindings set consists of `<identifier>`, and the value this identifier is bound to rolls through the whole list. `<expr>` is evaluated once per each traversal.

#### Example:
```scheme
(do-for ((x (begin (println "starting x") '(1 2 3))) (y (begin (println "starting y") '(1 2 3)))) (println x " * " y " = " (* x y)))                ===>
|starting x|
|starting y|
|1 * 1 = 1 |
|1 * 2 = 2 |
|1 * 3 = 3 |
|starting y|
|2 * 1 = 2 |
|2 * 2 = 4 |
|2 * 3 = 6 |
|starting y|
|3 * 1 = 3 |
|3 * 2 = 6 |
|3 * 3 = 9 |
```
### 2. Pattern list generator
auxiliary syntax: `(<pattern> <- <expr>)`

`<expr>` should evaluate to a list. Bindings set consists of pattern variables of `<pattern>`. Every element of the list, during traversal, is matched against the pattern; it is an error if there's no match. Binding set consists of all pattern variables and corresponding (sub...)expressions. `<expr>` is evaluated once per each traversal.

#### Example:
```scheme
(for (((string a b . _) <- '("Hello," "world!"))) (string a b))                ===> ("He" "wo")
```
### 3. let-generators
auxiliary syntax: `(let (<identifier1> <init1>) (<identifier2> <init2>)...)`

auxiliary syntax: `(let* (<identifier1> <init1>) (<identifier2> <init2>)...)`

auxiliary syntax: `(letrec (<identifier1> <init1>) (<identifier2> <init2>)...)`

Formally, the traversal of let-generator always produces a single value. It does not have an underlying actual list, but rather creates, in the sense of `(let)`, `(let*)` or `(letrec)`, respectively, fresh bindings for variables. Bindings set consists of all the identifiers in `<bindings>`, and the values they are bound to.

In other words,
```scheme
(for (<generator1>... (let <binding1> <binding2>...) <generator_m>...) <expr>...)
```
is identical to
```scheme
(for (<generator1>...) (let (<binding1> <binding2>...) (concat (for (<generator_m> ...) <expr>...))))
```, and
```scheme
(do-for (<generator1>... (let <binding1> <binding2>...) <generator_m>...) <expr>...)
```
is identical to
```scheme
(do-for (<generator1>...) (let (<binding1> <binding2>...) (do-for (<generator_m>...) <expr>...)))
```. So is with `(let*)` and `(letrec)`.

`(for)` form with only let-generators produces a singleton list, and `(do-for)` with only let-generators is equivalent to nested let-forms with the same `<bindings>` and `<expr>`s.

Rationale: let-generators are but an auxiliary syntax, and parentheses with let keyword inside cannot contain anything but bindings, no `<expr>`s to execute. Thus binding are not taken into additional parentheses, to reduce the amount of unneeded parentheses, albeit being in contradiction with standard library `(let)` syntax.

#### Example:
```scheme
(for ((x '(1 2 3)) (let (y (* x 2)))) y)                ===> (2 4 6)
(for ((let (x 12))) x)                                  ===> (12)
```
### 4. &lt;-let-generators
auxiliary syntax: `(<-let (<pattern1> <init1>) (<pattern2> <init2>)...)`

auxiliary syntax: `(<-let* (<pattern1> <init1>) (<pattern2> <init2>)...)`

Formally, the traversal of &lt;-let-generator always produces a single value. It does not have an underlying actual list, but rather creates, in the sense of `(<-let)` or `(<-let*)`, respectively, fresh bindings for pattern variables. Bindings set consists of all the pattern variables in `<bindings>`, and the (sub...)expressions they are bound to.

In other words, `(for (<generator1>... (<-let <pattern-binding1> <pattern-binding2>...) <generator_m>...) <expr>...)` is identical to `(for (<generator1>...) (<-let (<pattern-binding1> <pattern-binding2>...) (concat (for (<generator_m> ...) <expr>...))))`, and `(do-for (<generator1>... (<-let <pattern-binding1> <pattern-binding2>) <generator_m>...) <expr>...)` is identical to `(do-for (<generator1>...) (<-let (<pattern-binding1> <pattern-binding2>...) (do-for (<generator_m>...) <expr>...)))`. So is with `(<-let*)`.

It is an error if an `<init>` does not match its corresponding `<pattern>`.

`(for)` form with only &lt;-let-generators produces a singleton list, and `(do-for)` with only &lt;-let-generators is equivalent to nested &lt;-let-forms with the same `<pattern-bindings>` and `<expr>`s.

#### Example:
```scheme
(do-for ((x '(1 2 3)) (<-let ((_ . xs) (seq-list x 4)))) (println x " : " xs))                ===>
|1 : (2 3)|
|2 : (3)  |
|3 : ()   |
(define a '(41 (42) 43))
(for ((<-let* ((_ b _) a) ((a . _) b))) a)                                                  ===> (42)
```
### 5. &lt;-if-let-generators
auxiliary syntax: `(<-if-let (<pattern1> <init1>) (<pattern2> <init2>)...)`

auxiliary syntax: `(<-if-let* (<pattern1> <init1>) (<pattern2> <init2>)...)`

Formally, the traversal of &lt;-if-let-generator always produces at most one value. It does not have an underlying actual list, but rather creates, in the sense of `(<-if-let)` or `(<-if-let*)`, respectively, fresh bindings for pattern variables. Bindings set consists of all the pattern variables in `<bindings>`, and the (sub...)expressions they are bound to. When an `<init>` does not match its `<pattern>`, &lt;-if-let-generator produces a null list.

In other words, `(for (<generator1>... (<-if-let <pattern-binding1> <pattern-binding2>...) <generator_m>...) <expr>...)` is identical to `(for (<generator1>...) (<-if-let (<pattern-binding1> <pattern-binding2>...) (concat (for (<generator_m> ...) <expr>...)) '()))`, and `(do-for (<generator1>... (<-if-let <pattern-binding1> <pattern-binding2>...) <generator_m>...) <expr>...)` is identical to `(do-for (<generator1>...) (<-if-let (<pattern-binding1> <pattern-binding2>) (do-for (<generator_m>...) <expr>...) #f))`. So is with `(<-if-let*)`. &lt;-if-let generator placed the last in the chain adds pattern variable bindings and filters out values from the resulting list/`(do-for)` execution order when there is no match.

`(for)` form with only &lt;-if-let-generators produces a singleton list, when all `<init>`s match their `<pattern>`s, and empty list otherwise.

#### Example:
```scheme
(for ((x '(1 #(2) #(3 0.14159) 4)) (<-if-let (#(a : _) x))) a)                    ===> (2 3)
(define words '("Hello" "Yellow" "Green" "Mellow" "Meow"))
(for ((w words) (<-if-let ((string c #\e #\l #\l #\o . _) w))) c)                 ===> (#\H #\Y #\M)
(define a #f)
(for ((<-if-let* (a 42))) a)                                                      ===> (42)
(for ((<-if-let* (42 a))) a)                                                      ===> ()
```
## III. Guards

Guards add filtering to the list buildup/`(do-for)` execution. Guards do the same to list comprehension, what `(map-if)` and `(map-while)`, and `(map-if-while)` and `(map-while-if)` do to mapping, and their `(for-each)` counterparts do to `(for-each)`.

Guards are injected inside list generator's parentheses. Currently, there is no such thing as guarded .\*let-generators. If you need guarded &lt;-if-let-generator you can use guarded pattern in it; and if you need guarded let- of &lt;-let-generator you can switch to &lt;-if-let-generator. Or, say, guarded let-generator with a single binding can be expressed like this: instead of `(let (x <expr>) ? <guard>)` use `(x (list <expr>) ? <guard>)`.

auxiliary syntax: `(<listgen> ? <expr1> <expr2>...)`

auxiliary syntax: `(<listgen> ?? <expr1> <expr2>...)`

`<listgen>` is either `<identifier> <list-expr>` or `<pattern> <- <list-expr>`, core of a list generator. For each value generated by `<listgen>`, `<expr>`s are evaluated (taking into considerations any bindings produced by generators to the left) and their values are used as filters. When guard `<expr>`s are attached to the generator source code via `?`, the values produced by the guarded generator are all the values produced by `<listgen>` that satisfy all the `<expr>`s, i.e. for which every `<expr>` has true value; with ??, the values produced by the guarded generator are all the initial values produced by `<listgen>` as long as they satisfy all the `<expr>`s.

To illustrate this, `(for ((x <list-expr> ? <g-expr1> <g-expr2>...)) <expr1> <expr2>...)` is identical to `(map-if (lambda (x) <expr1> <expr2>...) (lambda (x) (and <g-expr1> <g-expr2>...)) <list-expr>)`, which in turn is equivalent to `(map (lambda (x) <expr1> <expr2>...) (filter (lambda (x) (and <g-expr1> <g-expr2>...)) <list-expr>))`; and if there were two question marks, it would be equivalent to the call to `(map-while)`, which in turn would be equivalent to mapping the function to the result of `(take-while)`.

#### Example:
```scheme
(define a '(1 2 3 4 5 6 7))
(for ((x a ? (odd? x))) x)                ===> (1 3 5 7)
(for ((x a ?? (< x 4))) x)                ===> (1 2 3)
```
auxiliary syntax: `(<listgen> ? <expr1> ?? <expr2>...)`

auxiliary syntax: `(<listgen> ?? <expr1> ? <expr2>...)`

Guards are applied right-to left: list produced by `<listgen>` is first filtered through right guard's expressions, and then the result is filtered through the left guard's only expression. The semantics of `?` and `??` is identical to that in the single-guard case: `??` marks while-guard, and `?` marks if-guard. This kind of guard is identical to using `(map-if-while)`, or `(map-while-if)`, or `(for-each-if-while)`, or `(for-each-while-if)`, depending on the for-form used.

#### Example:
```scheme
(define xs '(1 2 3 4 5 8 9 10 6 7))
(for ((x xs ? (odd? x) ?? (< x 7))) x)                ===> (1 3 5)
(set! xs '(1 8 3 10 5 2 9 4 6 7))
(for ((x xs ?? (odd? x) ? (< x 7))) x)                ===> (1 3 5)
```
