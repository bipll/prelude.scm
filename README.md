# prelude.scm - a general-purpose R5RS-compliant Scheme library.

Authors
=======

Written by Denis Trapeznikov, denis.v.trapeznikov@mail.ru.

License
=======

This software is licensed under the terms of GNU Public License v. 3 (see COPYING file).

Installation
============

R5RS does not define any portable library mechanism, so **prelude.scm** comes in the form of plain source code. You can use it any way your implementation of choice allows it. I was using Gambit-C Scheme Interpreter, while working on the library, and had it simply like this:
```bash
alias gsi='gsi -:s /home/bipll/prelude.scm/prelude.scm'
```
Some implementations may allow to pre-compile it as a library, it would be handy to do so.
There is an additional file **pr6lude.scm**. It contains the same definitions contained in a wrapping (library) form, defined in R6RS, with all the exports declared. You can use it as a library in your projects.

Usage
=====

**prelude.scm**, named after Haskell's Prelude, contains some definitions that might prove useful: pattern-matching forms, list comprehension, combinatory logic primitives, etc. There are additional README files in this archive. They are, in recommended order of reading:

- **[README.index](README.index.md)**         - thematic reference of all the identifiers bound inside the library;
- **[README.patterns](README.patterns.md)**      - the description of pattern-mathing concepts inside the library;
- **[README.comprehension](README.comprehension.md)** - the description of list comprehension syntax inside the library.

README.index describes all the procedures and syntax forms, divided in sections. It is organized as if it was a real amendment to the R5RS, and all the identifiers are distributed among R5RS paragraphs they could really belong to.

In all the three README files, the examples section follows R5RS in its syntax, with expression value adjoined with `===>`, and additionally with program output, if any, appended to the example line, framed in vertical bars, like this:
```scheme
(begin (display "Line 1 of output") (newline) (display "Another line of output") (newline) '(42))                ===> (42)
|Line 1 of output      |
|Another line of output|
```

Example
=======
```scheme
(define (improper-length container)
  (letrec
    ((i-l (<-case-lambda					; `<-' denotes a pattern-matching form
	    ((accum (_ . ls)) (i-l (+ accum 1) ls))		; in case of non-empty list
	    ((accum #(_ : vs)) (i-l (+ accum 1) vs))		; in case of non-empty vector
	    ((accum (string _ : ss)) (i-l (+ accum 1) ss))	; in case of non-empty string
	    ((accum _) accum))))				; in case of any other object
    (i-l 0 container)))

(improper-length "Hello, world!")                                                     ===> 13
(improper-length 42)                                                                  ===> 0

(do-for ((x '(1 2 3)) (y '(4 5 6))) (println x " x " y " = " (* x y)))                ===>
|1 x 4 = 4 |
|1 x 5 = 5 |
|1 x 6 = 6 |
|2 x 4 = 8 |
|2 x 5 = 10|
|2 x 6 = 12|
|3 x 4 = 12|
|3 x 5 = 15|
|3 x 6 = 18|
```
